CREATE SCHEMA COWPAT ;

CREATE TABLE COWPAT.TICKETS (
  ID INT NOT NULL ,
  PRENAME VARCHAR(48) NOT NULL ,
  SURNAME VARCHAR(48) NOT NULL ,
  LOCATION VARCHAR(48) NOT NULL ,
  PRIMARY KEY (ID)
) ENGINE = InnoDB;

CREATE TABLE COWPAT.TICKETMAP (
  ROUND INT NOT NULL,
  TICKET_ID INT NOT NULL ,
  FIELD_ID INT NOT NULL ,
  PRIMARY KEY (ROUND, TICKET_ID)
) ENGINE = InnoDB;

CREATE TABLE COWPAT.FIELDLOG (
  ID INT NOT NULL AUTO_INCREMENT,
  ROUND INT NOT NULL,
  FIELD_ID INT NOT NULL,
  FIELD_ENTERED BIGINT NULL,
  FIELD_LEFT BIGINT NULL,
  PRIMARY KEY (ID)
) ENGINE = InnoDB;

CREATE TABLE COWPAT.INFO (
  ROUND INT NOT NULL,
  NAME varchar(48) NOT NULL,
  VALUE BIGINT NOT NULL,
  PRIMARY KEY (ROUND, NAME)
) ENGINE=InnoDB;

CREATE TABLE COWPAT.INSTRUCTIONS (
  NAME varchar(48) NOT NULL,
  VALUE varchar(255) NOT NULL,
  PRIMARY KEY (NAME)
) ENGINE=InnoDB;

INSERT INTO COWPAT.INSTRUCTIONS(NAME,VALUE)
VALUES('page','main'),('searchparams',''),('round','0');


/*
C:\Programme\xampp\htdocs\kfr>..\..\mysql\bin\mysqldump.exe --default-character-set=utf8 -uroot -rcowpat_dump2.sql cowpat 
C:\Programme\xampp\htdocs\kfr>..\..\mysql\bin\mysql.exe --default-character-set=utf8 -uroot cowpat < cowpat_dump.sql
*/

/*
--Mehr Testdaten erzeugen

use cowpat;

create table prename(PRENAME VARCHAR(48) NOT NULL);
create table surname(SURNAME VARCHAR(48) NOT NULL);
create table location(LOCATION VARCHAR(48) NOT NULL);

insert into prename(PRENAME)
select distinct PRENAME
from    TICKETS;

insert into surname(SURNAME)
select distinct SURNAME
from    TICKETS;

insert into location(LOCATION)
select distinct LOCATION
from    TICKETS;

alter table TICKETS modify ID INT NOT NULL AUTO_INCREMENT;

insert into TICKETS(PRENAME,SURNAME,LOCATION)
select  prename.PRENAME,surname.SURNAME,location.LOCATION
from    prename,surname,location
limit 5000;

alter table TICKETS modify ID INT NOT NULL;

drop table prename;
drop table surname;
drop table location;
*/