#! /usr/bin/env python
#
# Generate a tickets.csv
#

import random
import sys

prenames = [
    # female
    "Mary","Anna","Emma","Elizabeth","Minnie","Margaret","Ida","Alice","Bertha","Sarah","Annie",
    "Clara","Ella","Florence","Cora","Martha","Laura","Nellie","Grace","Carrie","Maude","Mabel",
    "Bessie","Jennie","Gertrude","Julia","Hattie","Edith","Mattie","Rose","Catherine","Lillian",
    "Ada","Lillie","Helen","Jessie","Louise","Ethel","Lula","Myrtle","Eva","Frances","Lena","Lucy",
    "Edna","Maggie","Pearl","Daisy","Fannie","Josephine","Dora","Rosa","Katherine","Agnes","Marie",
    "Nora","May","Mamie","Blanche","Stella","Ellen","Nancy","Effie","Sallie","Nettie","Della",
    "Lizzie","Flora","Susie","Maud","Mae","Etta","Harriet","Sadie","Caroline","Katie","Lydia",
    "Elsie","Kate","Susan","Mollie","Alma","Addie","Georgia","Eliza","Lulu","Nannie","Lottie",
    "Amanda","Belle","Charlotte","Rebecca","Ruth","Viola","Olive","Amelia","Hannah","Jane",
    "Virginia","Emily","Matilda","Irene","Kathryn","Esther","Willie","Henrietta","Ollie","Amy",
    "Rachel","Sara","Estella","Theresa","Augusta","Ora","Pauline","Josie","Lola","Sophia",
    "Leona","Anne","Mildred","Ann","Beulah","Callie","Lou","Delia","Eleanor","Barbara","Iva",
    "Louisa","Maria","Mayme","Evelyn","Estelle","Nina","Betty","Marion","Bettie","Dorothy",
    "Luella","Inez","Lela","Rosie","Allie","Millie","Janie","Cornelia","Victoria","Ruby",
    "Winifred","Alta","Celia","Christine","Beatrice","Birdie","Harriett","Mable","Myra",
    "Sophie","Tillie","Isabel","Sylvia","Carolyn","Isabelle","Leila","Sally","Ina","Essie",
    "Bertie","Nell","Alberta","Katharine","Lora","Rena","Mina","Rhoda","Mathilda","Abbie",
    "Eula","Dollie","Hettie","Eunice","Fanny","Ola","Lenora","Adelaide","Christina","Lelia",
    "Nelle","Sue","Johanna","Lilly","Lucinda","Minerva","Lettie","Roxie","Cynthia","Helena",
    "Hilda","Hulda","Bernice","Genevieve","Jean","Cordelia","Marian","Francis","Jeanette",
    # male
    "John","William","James","Charles","George","Frank","Joseph","Thomas","Henry","Robert",
    "Edward","Harry","Walter","Arthur","Fred","Albert","Samuel","David","Louis","Joe","Charlie",
    "Clarence","Richard","Andrew","Daniel","Ernest","Will","Jesse","Oscar","Lewis","Peter",
    "Benjamin","Frederick","Willie","Alfred","Sam","Roy","Herbert","Jacob","Tom","Elmer","Carl",
    "Lee","Howard","Martin","Michael","Bert","Herman","Jim","Francis","Harvey","Earl","Eugene",
    "Ralph","Ed","Claude","Edwin","Ben","Charley","Paul","Edgar","Isaac","Otto","Luther",
    "Lawrence","Ira","Patrick","Guy","Oliver","Theodore","Hugh","Clyde","Alexander","August",
    "Floyd","Homer","Jack","Leonard","Horace","Marion","Philip","Allen","Archie","Stephen",
    "Chester","Willis","Raymond","Rufus","Warren","Jessie","Milton","Alex","Leo","Julius","Ray",
    "Sidney","Bernard","Dan","Jerry","Calvin","Perry","Dave","Anthony","Eddie","Amos","Dennis",
    "Clifford","Leroy","Wesley","Alonzo","Garfield","Franklin","Emil","Leon","Nathan","Harold",
    "Matthew","Levi","Moses","Everett","Lester","Winfield","Adam","Lloyd","Mack","Fredrick","Jay",
    "Jess","Melvin","Noah","Aaron","Alvin","Norman","Gilbert","Elijah","Victor","Gus","Nelson",
    "Jasper","Silas","Jake","Christopher","Mike","Percy","Adolph","Maurice","Cornelius","Felix",
    "Reuben","Wallace","Claud","Roscoe","Sylvester","Earnest","Hiram","Otis","Simon","Willard",
    "Irvin","Mark","Jose","Wilbur","Abraham","Virgil","Clinton","Elbert","Leslie","Marshall",
    "Owen","Wiley","Anton","Morris","Manuel","Phillip","Augustus","Emmett","Eli","Nicholas",
    "Wilson","Alva","Harley","Newton","Timothy","Marvin","Ross","Curtis","Edmund","Jeff","Elias",
    "Harrison","Stanley","Columbus","Lon","Ora","Ollie","Pearl","Russell","Solomon","Arch","Asa",
    "Clayton","Enoch","Irving","Mathew","Nathaniel","Scott","Hubert","Lemuel","Andy","Emanuel"
]

surnames = [
    "Smith","Johnson","Williams","Brown","Jones","Miller","Davis","Garcia","Rodriguez","Wilson",
    "Martinez","Anderson","Taylor","Thomas","Hernandez","Moore","Martin","Jackson","Thompson",
    "White","Lopez","Lee","Gonzalez","Harris","Clark","Lewis","Robinson","Walker","Perez","Hall",
    "Young","Allen","Sanchez","Wright","King","Scott","Green","Baker","Adams","Nelson","Hill",
    "Ramirez","Campbell","Mitchell","Roberts","Carter","Phillips","Evans","Turner","Torres",
    "Parker","Collins","Edwards","Stewart","Flores","Morris","Nguyen","Murphy","Rivera","Cook",
    "Rogers","Morgan","Peterson","Cooper","Reed","Bailey","Bell","Gomez","Kelly","Howard","Ward",
    "Cox","Diaz","Richardson","Wood","Watson","Brooks","Bennett","Gray","James","Reyes","Cruz",
    "Hughes","Price","Myers","Long","Foster","Sanders","Ross","Morales","Powell","Sullivan",
    "Russell","Ortiz","Jenkins","Gutierrez","Perry","Butler","Barnes","Fisher","Henderson",
    "Coleman","Simmons","Patterson","Jordan","Reynolds","Hamilton","Graham","Kim","Gonzales",
    "Alexander","Ramos","Wallace","Griffin","West","Cole","Hayes","Chavez","Gibson","Bryant",
    "Ellis","Stevens","Murray","Ford","Marshall","Owens","Mcdonald","Harrison","Ruiz","Kennedy",
    "Wells","Alvarez","Woods","Mendoza","Castillo","Olson","Webb","Washington","Tucker","Freeman",
    "Burns","Henry","Vasquez","Snyder","Simpson","Crawford","Jimenez","Porter","Mason","Shaw",
    "Gordon","Wagner","Hunter","Romero","Hicks","Dixon","Hunt","Palmer","Robertson","Black",
    "Holmes","Stone","Meyer","Boyd","Mills","Warren","Fox","Rose","Rice","Moreno","Schmidt",
    "Patel","Ferguson","Nichols","Herrera","Medina","Ryan","Fernandez","Weaver","Daniels",
    "Stephens","Gardner","Payne","Kelley","Dunn","Pierce","Arnold","Tran","Spencer","Peters",
    "Hawkins","Grant","Hansen","Castro","Hoffman","Hart","Elliott","Cunningham","Knight",
    "Bradley","Carroll","Hudson","Duncan","Armstrong","Berry","Andrews","Johnston","Ray","Lane",
    "Riley","Carpenter","Perkins","Aguilar","Silva","Richards","Willis","Matthews","Chapman",
    "Lawrence","Garza","Vargas","Watkins","Wheeler","Larson","Carlson","Harper","George","Greene",
    "Burke","Guzman","Morrison","Munoz","Jacobs","Obrien","Lawson","Franklin","Lynch","Bishop",
    "Carr","Salazar","Austin","Mendez","Gilbert","Jensen","Williamson","Montgomery","Harvey",
    "Oliver","Howell","Dean","Hanson","Weber","Garrett","Sims","Burton","Fuller","Soto","Mccoy",
    "Welch","Chen","Schultz","Walters","Reid","Fields","Walsh","Little","Fowler","Bowman",
    "Davidson","May","Day","Schneider","Newman","Brewer","Lucas","Holland","Wong","Banks","Santos",
    "Curtis","Pearson","Delgado","Valdez","Pena","Rios","Douglas","Sandoval","Barrett","Hopkins",
    "Keller","Guerrero","Stanley","Bates","Alvarado","Beck","Ortega","Wade","Estrada","Contreras",
    "Barnett","Caldwell","Santiago","Lambert","Powers","Chambers","Nunez","Craig","Leonard","Lowe",
    "Rhodes","Byrd","Gregory","Shelton","Frazier","Becker","Maldonado","Fleming","Vega","Sutton",
    "Cohen","Jennings","Parks","Mcdaniel","Watts","Barker","Norris","Vaughn","Vazquez","Holt",
    "Schwartz","Steele","Benson","Neal","Dominguez","Horton","Terry","Wolfe","Hale","Lyons",
    "Graves","Haynes","Miles","Park","Warner","Padilla","Bush","Thornton","Mccarthy","Mann",
    "Zimmerman","Erickson","Fletcher","Mckinney","Page","Dawson","Joseph","Marquez","Reeves",
    "Klein","Espinoza","Baldwin","Moran","Love","Robbins","Higgins","Ball","Cortez","Le",
    "Griffith","Bowen","Sharp","Cummings","Ramsey","Hardy","Swanson","Barber","Acosta","Luna",
    "Chandler","Blair","Daniel","Cross","Simon","Dennis","Oconnor","Quinn","Gross","Navarro",
    "Moss","Fitzgerald","Doyle","Mclaughlin","Rojas","Rodgers","Stevenson","Singh","Yang"
];

locations = [
    "New York", "Austin", "Chicago", "Los Angeles", "Miami", "San Francisco", "Orlando",
    "Key West", "Seattle", "Indianapolis", "Dallas", "Houston", "Denver", "Toronto", "Nashville",
    "Pittsburgh", "Philadelphia", "Boston", "Columbus", "Atlanta", "Washington", "Kansas City",
    "Omaha", "Des Moines", "Milwaukee", "Minneapolis", "New Orleans", "Detroit", "Buffalo",
    "New Jersey", "Wilmington", "Louisville", "Cincinnati", "Huntsville", "Jacksonville",
    "Tampa", "Salt Lake City", "Portland", "Las Vegas", "Sacramento", "Phoenix", "Charleston",
    "Albuquerque", "Tucson", "Reno", "Oklahoma City", "Calgary", "Tacoma", "Vancouver", "Biloxi",
    "Jackson", "Boise", "Rochester", "Fargo", "Winnipeg", "San Diego", "San José", "Fort Worth",
    "San Antonio", "Amarillo", "Santa Fe", "Wichita", "Colorado Springs", "Mesa", "Durham",
    "Memphis", "Norfolk", "Shreveport", "St. Louis", "Evansville", "Knoxville", "Springfield",
    "Odessa", "Las Cruces", "Fort Wayne", "Toledo", "Ann Arbor", "Cedar Rapids", "Rockford",
    "Cleveland", "Naperville", "Madison", "Charlottesville", "Providence", "Albany", "Lexington",
    "Montreal", "Ottawa", "Bloomington", "Champaign", "Bismarck", "Billings", "Miles City",
    "Auburn", "Hamilton", "Amory", "Grenada", "Newton", "Meridian", "Grove Hill", "Monroeville",
    "Camden", "Troy", "Luverne", "Abbeville", "Leesburg", "Wetumpka", "Tuskegee", "Americus",
    "Dawson", "Griffin", "Athens", "Marietta", "Rome", "Calhoun", "Fulton", "Clarksdale",
    "Belzoni", "Yazoo City", "Winona", "Batesville", "New Albany", "Quitman", "Magee", "Destin",
    "Brookhaven", "Carthage", "Fayette", "Trussville", "Ozark", "Eufaula", "Perry", "Atmore",
    "Fort Benning", "Hartselle", "Cullman", "Fort Payne", "Guntersville", "Albertville", "Chipley",
    "Gulfport", "Bogalusa", "Slidell", "Union Springs", "Panama City", "Crestview", "Tallahassee"
];

# Main function
def main(args):
    ticketCount = 30000
    ticketStart = 10000
    ticketPerPersonMax = 20
    ticketId = ticketStart

    ticketFile = open('tickets.csv', 'w')

    while ticketCount > 0:
        if ticketCount < ticketPerPersonMax:
            ticketPerPersonMax = ticketCount
        ticketCountPerson = random.randint(1, ticketPerPersonMax)
        ticketCount -= ticketCountPerson

        prename = random.choice(prenames)
        surname = random.choice(surnames)
        location = random.choice(locations)

        ticketIdEnd = ticketId + ticketCountPerson

        while ticketId < ticketIdEnd:
            ticketFile.write("%d;%s;%s;%s\n"%(
                ticketId, prename, surname, location))
            ticketId += 1

        ticketId = ticketIdEnd

    ticketFile.close()

# Main, main, main, main, mainly main, main, main, main!
if __name__ == "__main__":
    main(sys.argv)
