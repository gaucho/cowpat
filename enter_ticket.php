<?php
include_once 'util/error.php';

include_once 'util/db.php';
include_once 'util/cowpat.php';
include_once 'util/parse.php';

$status = "";

// Process request if submit was clicked
if ( isset($_POST['submit']) ) {
    $tickets = array();
    $ticketstring = htmlentities(trim($_POST['ticket']), ENT_NOQUOTES, "UTF-8");
    $prename = htmlentities(trim($_POST['prename']), ENT_NOQUOTES, "UTF-8");
    $surname = htmlentities(trim($_POST['surname']), ENT_NOQUOTES, "UTF-8");
    $location = htmlentities(trim($_POST['location']), ENT_NOQUOTES, "UTF-8");

    // Check metadata
    $status = "";
    $error = parse_ticketString($tickets, $ticketstring, ",");
    if ( strlen($ticketstring) == 0 || strlen($error) > 0 ) {
        $status = "
<li>Ung&uuml;ltige Losnummernangabe '" . $ticketstring . "'</li>";
    }
    if (strlen($prename) == 0 && strlen($surname) == 0) {
        $status = $status . "
<li>Kein Name angegeben</li>";
    }
    if (strlen($location) == 0) {
        $status = $status . "
<li>Kein Ort angegeben</li>";
    }

    if (strlen($status) > 0) {
        $status = "<div class=\"error\">
<h3>Fehler bei der Loseingabe</h3>
<ul>
" . $status . "
</ul>
</div>";
    } else {
        $error = "";
        $dbconn = db_connect();
        if (!$dbconn) {
            $error = "Datenbankverbindung fehlgeschlagen!";
        } else {
            $error = cowpat_addTickets(
                $dbconn, $prename, $surname, $location, $tickets);
            db_disconnect($dbconn);
        }
        if (strlen($error) != 0) {
            $status = "<div class=\"error\">
<h3>Fehler beim Eintragen der Lose " . $ticketstring . ":</h3>
<br/>$error
</div>";
        } else {
            $status = $status . "<div class=\"success\">
<h3>Loseingabe erfolgreich</h3>
  Die Losnummern " . $ticketstring . " wurden erfolgreich eingetragen.
</div>";
        }
    }
}

// Get layout
$layout = join("", file("html/layout.html"));
$content = join("", file("html/enter_ticket.html"));

// Enter content
$layout = str_replace("{phpinput}", $content, $layout);
$layout = str_replace("{phpstatus}", $status, $layout);

// Print site
print $layout;
?>
