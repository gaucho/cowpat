<?php
include_once 'util/error.php';
include_once 'util/db.php';
include_once 'util/cowpat.php';
include_once 'util/parse.php';

$allCsvOption = "-- ALLE .csv Dateien --";
$status = "";

// Process request if submit was clicked
if ( isset($_POST['submit']) ) {
    $time = microtime(TRUE);

    $fileoption = $_POST['filename'];
    $filearray = array();

    $delimiter = $_POST['delimiter'];
    $enclosure = $_POST['enclosure'];
    $encoding = $_POST['encoding'];

    $statusError = "";
    $statusSuccess = "";

    $dbconn = db_connect();
    if (!$dbconn) {
        $statusError .= "<p>Datenbankverbindung ist gescheitert</p>";
    } else if ($fileoption === $allCsvOption) {
        if ($dh = opendir('data')) {
            while (($file = readdir($dh)) !== false) {
                if (substr($file, strlen($file)-strlen('.csv')) === '.csv' )
                    $filearray[] = $file;
            }
            closedir($dh);
        }
    } else {
        $filearray[] = $fileoption;
    }

    foreach($filearray as $filename) {
        $file = fopen("data/$filename","r");
        if($file == FALSE) {
            $statusError .= "<p>Datei $filename: Datei nicht gefunden!</p>
    ";
            continue;
        }

        // Collect tickets until ticket owner changes
        $tickets = array();
        $prename = "";
        $surname = "";
        $location = "";

        $line = 0;
        $tickets_empty = array();
        $tickets_nolocation = array();
        $tickets_noname = array();

        // String performs better than array (about 2 s at 10000 rows).
        // This is probably due to lots of array_merge calls.
        //$tickets_imported = array();
        $tickets_imported_count = 0;
        $tickets_imported_string = "";

        while( ($data = fgetcsv($file, 192, $delimiter)) !== FALSE) {
            $line += 1;
            $columns = count($data);
            if ($columns === 0
                    || ($columns === 1 && empty(trim($data[0])))) {
                continue;
            } else if ($columns < 4) {
                $statusError .= "<p>$filename: Zeile $line hat zu wenig ".
                                "Spalten !</p>
    ";
                continue;
            }

            $lineid       = htmlentities(trim($data[0]),ENT_NOQUOTES,$encoding);
            $lineprename  = htmlentities(trim($data[1]),ENT_NOQUOTES,$encoding);
            $linesurname  = htmlentities(trim($data[2]),ENT_NOQUOTES,$encoding);
            $linelocation = htmlentities(trim($data[3]),ENT_NOQUOTES,$encoding);

            // Check metadata
            $linetickets = array();
            $error = parse_ticketString($linetickets, $lineid, ",");
            if ( empty($lineid) || !empty($error) ) {
                $statusError .=  "<p>$filename: ".
                    ": Ung&uuml;ltige Ticketangabe ('$lineid') in " .
                    "Zeile $line</p>
    ";
                continue;
            }

            $hasprename = !empty($lineprename);
            $hassurname = !empty($linesurname);
            $haslocation = !empty($linelocation);

            if (!$hassurname && !$hasprename) {
                if ($haslocation) {
                    $tickets_noname[] = $lineid;
                } else {
                    $tickets_empty[] = $lineid;
                }
                continue;
            } else if (empty($linelocation)) {
                $tickets_nolocation[] = $lineid;
                continue;
            }

            if ($prename == $lineprename
                    && $surname == $linesurname
                    && $location == $linelocation) {
                // Same ticket owner as before add to tickets
                $tickets = array_merge($tickets, $linetickets);
            } else {
                // Different ticket owner as before, add saved tickets
                if (!empty($tickets)) {
                    $error = cowpat_addTickets(
                        $dbconn, $prename, $surname, $location, $tickets);
                    if (!empty($error)) {
                            $statusError .=  "<p>$filename: "
                                . ": Import von Los "
                                . parse_ticketArray($tickets)
                                . "fehlgeschlagen:<br />$error</p>
    ";
                    } else {
                        if ($tickets_imported_count > 0)
                            $tickets_imported_string .= ", ";
                        $tickets_imported_string .= parse_ticketArray($tickets);
                        $tickets_imported_count += count($tickets);
                        //$tickets_imported = array_merge($tickets_imported, $tickets);
                    }
                }

                $prename = $lineprename;
                $surname = $linesurname;
                $location = $linelocation;
                $tickets = $linetickets;
            }
        } // while( ($data = fgetcsv($file, 192, $delimiter)) !== FALSE)

        // Check if there are still tickets to add
        if (!empty($tickets)) {
            $error = cowpat_addTickets(
                $dbconn, $prename, $surname, $location, $tickets);
            if (!empty($error)) {
                    $ticketstring = parse_ticketArray($tickets);
                    $statusError .=  "<p>$filename: ".
                ": Import von Los $ticketstring fehlgeschlagen:<br />$error</p>
    ";
            } else {
                if ($tickets_imported_count > 0)
                    $tickets_imported_string .= ", ";
                $tickets_imported_string .= parse_ticketArray($tickets);
                $tickets_imported_count += count($tickets);
                //$tickets_imported = array_merge($tickets_imported, $tickets);
            }
        }

        $tickets_empty_count = count($tickets_empty);
        if ($tickets_empty_count > 0) {
            $statusError .=  "<p>$filename: "
                . ": Los " . parse_ticketArray($tickets_empty)
                . " enth&auml;lt keine Daten (gesamt: "
                . $tickets_empty_count . " Los(e))</p>
    ";
        }

        $tickets_noname_count = count($tickets_noname);
        if ($tickets_noname_count > 0) {
            $statusError .=  "<p>$filename: "
                . ": Bei Los " . parse_ticketArray($tickets_noname)
                . " ist kein Name angegeben (gesamt: "
                . $tickets_noname_count . " Los(e))</p>
    ";
        }

        $tickets_nolocation_count = count($tickets_nolocation);
        if ($tickets_nolocation_count > 0) {
            $statusError .=  "<p>$filename: "
                . ": Bei Los " . parse_ticketArray($tickets_nolocation)
                . " ist kein Ort angegeben (gesamt: "
                . $tickets_nolocation_count . " Los(e))</p>
    ";
        }

        if ($tickets_imported_count > 0) {
            $statusSuccess .=  "<p>$filename: "
                . ": Los ". $tickets_imported_string
                . " wurde erfolgreich importiert (gesamt: "
                . $tickets_imported_count . " Los(e))</p>
    ";
        }
    } // foreach($filearray as $filename)

    if ($dbconn) {
        db_disconnect($dbconn);
    }

    if (!empty($statusError)) {
        $status .= "<div class=\"error\">
<h3>Fehler beim Losimport</h3>
    $statusError
</div>";
    }
    if (!empty($statusSuccess)) {
        $status .= "<div class=\"success\">
<h3>Import erfolgreich</h3>
    $statusSuccess
</div>";
    }

    $duration = microtime(TRUE) - $time;
    $status .= "<div class=\"info\">
        Importdauer: $duration s
    </div>";
} // if submit is set

// Get files in directory data
$fileoptions = "<option>$allCsvOption</option>
    ";
if ($dh = opendir('data')) {
    while (($file = readdir($dh)) !== false) {
        if (substr($file, strlen($file)-strlen('.csv')) === '.csv' )
            $fileoptions .=  "<option>$file</option>
    ";
    }
    closedir($dh);
}

// Get layout
$layout = join("", file("html/layout.html"));
$content = join("", file("html/import_tickets.html"));

// Enter content
$layout = str_replace("{phpinput}", $content, $layout);
$layout = str_replace("{phpstatus}", $status, $layout);
$layout = str_replace("{phpcsvfiles}", $fileoptions, $layout);

// Print site
print $layout;
?>
