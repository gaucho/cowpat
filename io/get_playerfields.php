<?php
    include_once '../util/error.php';
    include_once '../util/db.php';

    $fieldArray = array();

    $prename  = isset( $_GET['prename'] ) ? $_GET['prename']  : "" ;
    $surname  = isset( $_GET['surname'] ) ? $_GET['surname']  : "" ;
    $location = isset( $_GET['location']) ? $_GET['location'] : "" ;
    $ticketid = isset( $_GET['ticketid']) ? $_GET['ticketid'] : "" ;
    $round    = isset( $_GET['round']   ) ? $_GET['round']    : 0    ;

    $dbconn = db_connect();
    
    $prename = db_realEscapeString( $dbconn, htmlentities(trim($prename),ENT_NOQUOTES, 'UTF-8'));
    $surname = db_realEscapeString( $dbconn, htmlentities(trim($surname),ENT_NOQUOTES, 'UTF-8')); 
    $location = db_realEscapeString( $dbconn, htmlentities(trim($location),ENT_NOQUOTES, 'UTF-8')); 
    $ticketid = db_realEscapeString( $dbconn, $ticketid );
    $round = db_realEscapeString( $dbconn, $round );    

    if( $round<=0 ) {
        return;
    }

    if( $ticketid!="" && is_numeric($ticketid))
    {
        $result = db_query($dbconn, "SELECT PRENAME,SURNAME,LOCATION FROM TICKETS WHERE ID=$ticketid");
        if(!$result) {
            echo db_getErrorMessage();
        } else if (db_getRowCount($result) == 1) {
            $row = db_fetchRow($result);
            $prename  = $row[0];
            $surname  = $row[1];
            $location = $row[2];
        }
    }

    if( $prename!="" && $surname!="" && $location!="" )
    {
        $sql = "SELECT TICKETMAP.FIELD_ID".
               " FROM TICKETS, TICKETMAP".
               " WHERE PRENAME='$prename'".
               " AND SURNAME='$surname'".
               " AND LOCATION='$location'".
               " AND TICKETMAP.TICKET_ID=TICKETS.ID".
               " AND TICKETMAP.ROUND=$round";
        $result = db_query( $dbconn, $sql );
        if(!$result) {
            echo db_getErrorMessage();
        } else if (db_getRowCount($result) > 0) {
            while ($row = db_fetchRow($result)) {
                array_push($fieldArray,$row[0]);
            }
        }
    }


    db_disconnect($dbconn);

    $result=array ('prename'=>$prename,'surname'=>$surname,'location'=>$location, 'fields'=>$fieldArray, 'round'=>$round);

    print json_encode($result);
?>
