<?php
    include_once '../util/error.php';
    include_once '../util/cowpat.php';

    $field = FALSE;
    $round = FALSE;

    if ( isset($_GET['field']) && is_numeric($field) ) {
        $field = (int)$_GET['field'];
    }
    if ( isset($_GET['round']) && is_numeric($_GET['round']) ) {
        $round = (int)$_GET['round'];
        $playerArray = array();
        $error = cowpat_getPlayersOnField($playerArray, $round, $field);
        if ($error == "") {
            print json_encode($playerArray);
        } else {
            print $error;
        }
    } else {
        print "numeric round parameter is required";
    }
?>
