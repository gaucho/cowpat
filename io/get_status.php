<?php
    include_once '../util/error.php';
    include_once '../util/db.php';
    include_once '../util/cowpat.php';

    $error = "";
    $round = 0;
    $field = 0;
    $rankingfield = 0;
    $instructionArray = array();
    $stateArray = array();
    $playerArray = array();
    $time = cowpat_getCurrentTime();

    $error = cowpat_getInstructions($instructionArray);
    if ($error == "") {
        if( isset($instructionArray['round']) && $instructionArray['round']>0 ) {
            $round = $instructionArray['round'];
        }
        else if( isset($_GET['round']) && is_numeric($_GET['round']) ) {
            //just for debug purposes, not used in production
            $round = $_GET['round'];
        }
        $instructionArray['now'] = $time;
    }

    if( $error=="" && $round>0 ) {
        $error = cowpat_getState($stateArray, $round);
        if ( isset($stateArray['field']) && $stateArray['field']>=0 ) {
            $field = $stateArray['field'];
            if ((int)$field == 0)
                $rankingfield = (int)$stateArray['oldfield'];
            else
                $rankingfield = $field;
        }
        else if( isset($_GET['field']) && is_numeric($_GET['field']) ) {
            //just for debug purposes, not used in production
            $field = $_GET['field'];
        }
    }

    if( $error=="" && $round>0 && $field>=0 ) {
        //$error = cowpat_getPlayersOnField($playerArray, $round, $field );
        $error = cowpat_getRankingOnField($playerArray, $round, $rankingfield, $time);

        $timeOnField = 0;
        if ($error == "")
            $error = cowpat_getTimeOnField($timeOnField, $round, $field, $time);
        $stateArray['expiredtime'] = $timeOnField;
        
        $timeOfGame = 0;
        if ($error == "")
            $error = cowpat_getGameTime($timeOfGame, $round, $time);
        $stateArray['gametime'] = $timeOfGame;
    }

    if( $error=="") {
        $result = array('instruction' => $instructionArray, 
                        'state' => $stateArray,
                        'player' => $playerArray );
        print json_encode($result);
    }
    else {
        print $error;
    }

?>
