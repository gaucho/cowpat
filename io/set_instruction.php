<?php
    include_once '../util/error.php';
    include_once '../util/db.php';
    include_once '../util/cowpat.php';

    $instruction = isset( $_GET['instruction'] ) ? $_GET['instruction'] : "";
    $round = isset( $_GET['round'] ) ? $_GET['round'] : 0;
    $details = isset( $_GET['details'] ) ? $_GET['details'] : null;

    if( $instruction=="" ) {
        echo "Instruction not set";
        exit;
    }

    //frontend instructions
    if( in_array($instruction, array('show_page','do_search','do_setround','time_back','round_reset')) ) {
        switch ($instruction) {
            case "show_page":
                    if( $details!=null )
                    echo cowpat_saveInstruction('page', $details);
                else
                    echo "Details empty";
                break;
            case "do_search":
                    if( $details!=null )
                    echo cowpat_saveInstruction('searchparams', $details);
                else
                    echo "Details empty";
                break;
            case "do_setround":
                if( $details!=null )
                    echo cowpat_saveInstruction('round', $details);
                else
                    echo "Details empty";
                break;
            case "time_back":
                if( $details!=null && is_numeric($details) && $details>0 )
                    echo cowpat_turnBackTime($details);
                else
                    echo "Details empty";
                break;
            case "round_reset":
                if( $details!=null && is_numeric($details) && $details>0 )
                    echo cowpat_resetRound($details);
                else
                    echo "Details empty";
                break;
        }
    }
    //game instructions
    else if($round>0) {
        switch ($instruction) {
            case "start_round":
                echo cowpat_startRound($round, $details);
                break;
            case "stop_round":
                echo cowpat_stopRound($round);
                break;
            case "pause_round":
                echo cowpat_pauseRound($round);
                break;
            case "continue_round":
                echo cowpat_continueRound($round);
                break;
            case "change_field":
                if( $details>0 )
                    echo cowpat_setField($round, $details);
                else
                    echo "Details empty";
                break;
            case "init_round":
                $initState = array();
                $initState['fieldcount'] = 192; // 192 fields
                $initState['fieldperiod'] = 10 * 1000; // 10 seconds
                $initState['maxtime'] = 10 * 60 * 1000; // 10 minutes
                $initState['winnercount'] = 3; // 3 players can win
                echo cowpat_initRound($round, $initState);
                break;
            default:
                echo "Unknown instruction";
        }
    }
    else {
        echo "Round not set or unknown frontend instruction";
    }

?>
