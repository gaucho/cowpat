<?php
    include_once '../util/error.php';
    include_once '../util/cowpat.php';

    if ( isset($_GET['round']) && is_numeric($_GET['round']) ) {
        $round = (int)($_GET['round']);
        $i = 1;
        $winnerArray = array();
        while( isset($_GET['w'.$i]) && is_numeric($_GET['w'.$i]) ) {
            $winner = (int)$_GET['w'.$i];
            $winnerArray[$winner] = $winner;
            $i += 1;
        }

        $error = cowpat_stopRound($round);
        if (strlen($error) === 0) {
            $error = cowpat_setWinners($round, $winnerArray);
        }
        print $error;
    } else {
        print "numeric round parameter is required";
    }

?>
