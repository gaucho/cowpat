function floatToInt(x) {
  var k=x.toString();
  var p=k.indexOf('.');
  if(p==-1)
    return x;
  return parseInt(k.substring(0,p));
}

var Person = Class.create();
Person.prototype = {
  initialize: function(id,prename,surname,location) {
    this.id = id;
    this.prename = prename;
    this.surname = surname;
    this.location = location;
  }
};

var GameState = Class.create();
GameState.prototype = {
    initialize: function( a ) {
        if( a!=null ) {
            this.endtime         = a['endtime'];
            this.field           = a['field'];
            this.fieldcount      = a['fieldcount'];
            //this.fieldentered    = a['fieldentered'];
            this.fieldleft       = a['fieldleft'];
            this.expiredtime     = a['expiredtime'];
            this.gametime        = a['gametime'];
            this.starttime       = a['starttime'];
        }
        else {
            this.endtime         = 0;
            this.field           = 0;
            this.fieldcount      = 192;
            //this.fieldentered    = 0;
            this.fieldleft       = 0;
            this.expiredtime     = 0;
            this.gametime        = 0;
            this.starttime       = 0;
        }
    },
    /*getOverflows: function( time ) {
        if(this.isStarted()) {
            var entered = this.getFieldentered();
            var overflows=floatToInt(Math.abs((time-entered)/(1000*10)));
            return overflows;
        } else {
            return 0;
        }
    },*/
    getEndtime: function() {
        return (this.endtime);
    },
    getStarttime: function() {
        return (this.starttime);
    },
    getExpiredTime: function() {
        return (this.expiredtime);
    },
    getGameTime: function() {
        return (this.gametime);
    },
    /*getFieldentered: function() {
        return ((this.fieldentered) - (this.expiredtime));
    },*/
    getField: function() {
        return this.field;
    },
    isStarted: function() {
        return this.starttime>0 ? true : false;
    },
    isFinished: function() {
        return this.endtime>0 ? true : false;
    }
};

var GameInstruction = Class.create();
GameInstruction.prototype = {
    initialize: function() {
        this.page='main';
        this.searchparams='';
        this.round=0;
        this.now=0;

        this.gamestate = null;
        this.player = null;
    },
    processInstruction: function() {
        var url = window.location.pathname;
        var filename = url.substring(url.lastIndexOf('/')+1);
        if(this.page==='main' && filename!='kfrmain.html') {
            window.location = 'kfrmain.html';
        }
        if(this.page=='search' && filename!='kfrspielersuche.html') {
            window.location = 'kfrspielersuche.html';
        }
        if(this.page=='idle' && filename!='kfridle.html') {
            window.location = 'kfridle.html';
        }
        if(this.page=='winners' && filename!='kfrgewinner.php') {
            window.location = 'kfrgewinner.php';
        }
    }
};

var globalGameInstruction = new GameInstruction();
var periodicalInstructionExecutor = new PeriodicalExecuter(updateGameInstruction, 3);

function updateGameInstruction() {
    var url = 'io/get_status.php';
    new Ajax.Request(url, {
        method: 'get',
        onComplete: function(transport) {
            if (200 == transport.status) {
                var response = transport.responseText.evalJSON(true);
                if(response!=null) {
                    globalGameInstruction.page          = response.instruction['page'];
                    globalGameInstruction.searchparams  = response.instruction['searchparams'];
                    globalGameInstruction.round         = response.instruction['round'];
                    globalGameInstruction.now           = response.instruction['now'];
                    globalGameInstruction.gamestate     = new GameState(response.state);
                    globalGameInstruction.player        = response.player;
                }
                globalGameInstruction.processInstruction();
            }
            else {
                alert("Fehler bei der Abfrage: " + transport.status);
            }
        }
    });
    
}
