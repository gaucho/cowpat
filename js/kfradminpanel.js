


var url = 'io/set_instruction.php';
var fieldnrold;
var configuredround=0;

/*$(document).observe('keydown', function (e) {
   if(!fieldnrold>0) {
      return;
   }
   switch (e.keyCode) {
      case Event.KEY_LEFT:
         changefield(fieldnrold-1);
         break;
      case Event.KEY_RIGHT:
         changefield(fieldnrold+1);
         break;
      case Event.KEY_UP:
         changefield(fieldnrold-16);
         break;
      case Event.KEY_DOWN:
         changefield(fieldnrold+16);
         break;
   }
});*/


function createfieldtable( ) {
    var fieldtable='';
    fieldtable+='<table class="field">';
        for(var rows=0; rows<12; rows++)
        {
            fieldtable+="<tr>";
            for(var cols=1; cols<=16; cols++)
            {
                var fieldnr=(rows*16+cols);
                fieldtable+='<td class="field" id="fieldnr' + fieldnr +'" onclick="changefield('+fieldnr+')">'
                fieldtable+=fieldnr;
                fieldtable+='</td>';
            }
            fieldtable+="</tr>";
        }
    fieldtable+="</table>";
    $('fieldtable').update(fieldtable);
}

function changefield( fieldnr ) {
    if($('fieldnr'+fieldnrold)!=null) {
        $('fieldnr'+fieldnrold).setStyle({backgroundColor: 'transparent'});
    }
   if($('fieldnr'+fieldnr)!=null) {
      $('fieldnr'+fieldnr).setStyle({backgroundColor: '#79b818'});
      fieldnrold=fieldnr;		
   }
    sendinstructions('change_field',fieldnr);
}

function onstart() {
    $('startbutton').setStyle({backgroundColor: '#79b818'});
    sendinstructions('start_round',fieldnrold);
}

function onpauseround() {
    $('pausebutton').setStyle({backgroundColor: '#79b818'});
    sendinstructions('pause_round',configuredround);
}

function onend() {
    $('endbutton').setStyle({backgroundColor: '#79b818'});
    sendinstructions('stop_round',configuredround);
}

function oncontinue() {
    $('continuebutton').setStyle({backgroundColor: '#79b818'});
    sendinstructions('continue_round',configuredround);
}

function onturnbacktime() {
    if (confirm("Rundenende wirklich um 1 Sekunde zuruecksetzen?")) { 
        sendinstructions('time_back',configuredround);
    }
}

function onresetround() {
    if (confirm("Runde wirklich zuruecksetzen?")) { 
        sendinstructions('round_reset',configuredround);
    }
}

function ongotosearch() {
    sendinstructions('show_page','search');
}

function ongotomain() {
    sendinstructions('show_page','main');
}

function ongotoidle() {
    sendinstructions('show_page','idle');
}

function ongotowinners() {
    sendinstructions('show_page','winners');
}

function onplayersearch() {
    var a = $('playersearch').serialize(true);
    a=encodeURIComponent( Object.toJSON(a) );
    sendinstructions('do_search', a);
}

function oninitializeround() {
    sendinstructions('init_round', 192);
}

function onconfiguredroundchange() {
    var r = $('configuredround').value;
    if( r==null || r<=0 ) {
        alert( 'Rundenangabe ungueltig' );
    }
    else {
        configuredround=r;
        sendinstructions('do_setround',configuredround);
    }
}

function sendinstructions( instruction, details ) {
    if( configuredround==0 ) {
        alert( 'Runde muss gesetzt sein, Anfrage abgebrochen' );
        return;
    }

    new Ajax.Request(url, {
        method: 'get',
        parameters: {'round': configuredround, 'instruction':instruction, 'details': details},
        onComplete: function(transport) {
            if (200 != transport.status) {
                alert("Fehler bei der Uebermittlung: " + transport.status );
            } else if(transport.responseText!="") {
                alert("Serverfehler: " + transport.responseText);   
            }
        }
    });
}

