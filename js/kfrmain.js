
var periodicalTimerExecutor = new PeriodicalExecuter(updategamestate, 1);
var displaystate = new Array();
var timeoffset = 0;


//aktualisiert den gamestate jede sekunde (z�hler verringern, instruktionen aus dem puffer holen)
function updategamestate() {
    var gameInstruction = Object.clone(globalGameInstruction);
    var jetzt;//=new Date().getTime();
    var htmlstr="";
    
    if( gameInstruction!=null && gameInstruction.gamestate!=null )
    {
        jetzt = gameInstruction.now;
        //var players = randomsort(gameInstruction.player.clone());
        var players = gameInstruction.player.clone();
        var state = gameInstruction.gamestate;

        if( state.isFinished() )
            jetzt = state.getEndtime();

        if( jetzt<state.getStarttime() )
            jetzt = state.getStarttime();

        // 2019/08/29 - �nderung auf 10 statt 15 Minuten
        // if( (jetzt-state.getStarttime()) >= 15*60*1000 )
        //    jetzt = state.getStarttime() + 15*60*1000 /* - 1 */;
        if( (jetzt-state.getStarttime()) >= 10*60*1000 )
            jetzt = state.getStarttime() + 10*60*1000 /* - 1 */;

        htmlstr=getTimerString(state, jetzt);
        if( htmlstr!=displaystate['timer'] ) {
            displaystate['timer'] = htmlstr;
            $('timer').update( htmlstr );
        }

        htmlstr=getCounterString(state, jetzt);
        if( htmlstr!=displaystate['counter'] ) {
            displaystate['counter'] = htmlstr;
            $('counter').update( htmlstr );
        }

        if (state.field > 0)
            htmlstr="Feld " + state.field;
        else
            htmlstr="";
        if( htmlstr!=displaystate['fieldnum'] ) {
            displaystate['fieldnum'] = htmlstr;
            $('fieldnum').update( htmlstr );
        }

        htmlstr=getWinnerList(jetzt, state, players, 0, 3);
        if( htmlstr!=displaystate['winnerlist0'] ) {
            displaystate['winnerlist0'] = htmlstr;
            $('winnerlist0').update( htmlstr );
        }
        
        htmlstr=getWinnerList(jetzt, state, players, 3, 6);
        if( htmlstr!=displaystate['winnerlist1'] ) {
            displaystate['winnerlist1'] = htmlstr;
            $('winnerlist1').update( htmlstr );
        }
        
        htmlstr=getWinnerList(jetzt, state, players, 9, 6);
        if( htmlstr!=displaystate['winnerlist2'] ) {
            displaystate['winnerlist2'] = htmlstr;
            $('winnerlist2').update( htmlstr );
        }
    }
}

function getTimerString( gamestate, jetzt ) {
    var diffms;
    var diffdate;

    /*if( gamestate.isStarted() )
        diffms = 15*60*1000 + gamestate.getStarttime() - jetzt;
    else
        diffms = 15*60*1000;
    */

    // 2019/08/29 - �nderung auf 10 statt 15 Minuten
    // maxtime = 15 * 60 * 1000;
    maxtime = 10 * 60 * 1000;
    if (gamestate.isStarted() == false)
        time = 0;
    else {
        time = gamestate.getGameTime();
        if (time > maxtime)
            time = maxtime;
    }
   
    diffms = maxtime - time;

    diffdate = new Date(diffms);

    var retval="";
    if(diffdate.getMinutes()>=10)
        retval=retval+diffdate.getMinutes()+":";
    else
        retval=retval+"0"+diffdate.getMinutes()+":";
    if(diffdate.getSeconds()<10)
        retval=retval+"0";
    retval=retval+diffdate.getSeconds();
    return retval;
}

function getCounterString( gamestate, jetzt ) {
    if (gamestate.field == 0)
        return "Pause"
    
    var diffs=10;
    if( gamestate.isStarted() ) {
        //var diffms = (jetzt - gamestate.getFieldentered() );
        var diffms = gamestate.getExpiredTime();
        //$('debug').update("diffms:" + diffms);
        if( diffms<0 )
            diffms=0;
        diffms=diffms%10000;
        diffs=Math.floor(diffms/1000);
        //diffs=Math.round(diffms/1000);
        diffs=10-diffs;
    }

    return diffs + " Sekunden"
}

function getWinnerList(jetzt, gamestate, players, startpos, numpos) {
    var retstr="";

    if( gamestate==null || players==null )
        return retstr;

    var overflows = 0;//gamestate.getOverflows(jetzt);
    var numpersons = players.length;
    overflows = overflows%players.length;
    var pos=(overflows*3+startpos)%numpersons;

    //$('debug').update("overflows:" + overflows + " pos:" + pos );
    if(startpos+numpos>numpersons)
        numpos=numpersons-startpos;

    for(var i=0; i<numpos; ++i)
    {
        if(pos>=numpersons)
            pos-=numpersons;
        retstr+=getWinnerString(players[pos]);
        pos++;
    }
    return retstr;
}

function getWinnerString(person) {
    var retstr="";
    if(person!=null) {
        var len = getPrintedNumChars( person.prename + person.surname );
        if( len > 20 )
            retstr+='<li style="font-size:14pt;">';
        else if( len > 17 )
            retstr+='<li style="font-size:17pt;">';
        else if( len > 13 )
            retstr+='<li style="font-size:20pt;">';
        else if( len > 9 )
            retstr+='<li style="font-size:22pt;">';
        else
            retstr+='<li>';
        retstr+=person.prename+' '+person.surname+'<br/><span class="ort">'+person.location+/*' [#' + person.id + ']' +*/ '</span></li>';
    }
    return retstr;
}

/*
function randomsort( players ) {
    var len=players.length;
    var newPlayers = new Array();

    while (len > 0) {
        var i = players[0].id % len;
        var player = players[i];
        players.splice(i,1);
        newPlayers.push(player);
        len = players.length;
    }

    return newPlayers;
}
*/

function generateRandomNumbers(seed, count, min, max ) {
    var prn = new Array();
    var a = 48271;
    var m = 2147483647;
    var q = m/a;
    var r = m%a;
    var oneOverM = 1.0 / m;

    for (i = 0; i < count; i++) {
        hi = seed / q;
        lo = seed % q;
        var test = a*lo-r*hi;
        if( test > 0 ) {
            seed = test;
        }
        else {
            seed = test + m;
        }
        prn[i] = Math.round((max-min) * (seed * oneOverM) + min);
    }
    return prn;
}

function getTimeOffset() {
    var url = 'io/get_microtime.php';
    var start = new Date().getTime();
    new Ajax.Request(url, {
        method: 'get',
        onComplete: function(transport) {
            if (200 == transport.status) {
                var servertime = transport.responseText;
                var jetzt=new Date().getTime();
                var delay=(jetzt-start)/2
                timeoffset = jetzt-servertime-delay;
            }
        }
    });   
}

function getPrintedNumChars( teststring ) {
    teststring = teststring.replace(/&(((a|o|u)uml)|(szlig));/gi, "1");
    return teststring.length;
}
