
function createfieldtable( ) {
    var fieldtable='';
    fieldtable+='<table border="0">';
    
        for(var rows=0; rows<12; rows++)
        {
            fieldtable+="<tr>";
            for(var cols=1; cols<=16; cols++)
            {
                var fieldnr=(rows*16+cols);
                fieldtable+='<td id="fieldnr' + fieldnr +'">'
                fieldtable+=fieldnr;
                fieldtable+='</td>';
            }
            fieldtable+="</tr>";
        }
    fieldtable+="</table>";
    $('fieldtable').update(fieldtable);
}

var periodicalInstructionExecutor = new PeriodicalExecuter(update, 2);
var oldsearchparams="";
function update() {
    if( globalGameInstruction.searchparams!=oldsearchparams )
    {
        oldsearchparams=globalGameInstruction.searchparams;
        var a  = decodeURIComponent( globalGameInstruction.searchparams ).evalJSON(true);
        createfieldtable( );
        markFields( a );
    }
}

function markFields( details ) {
    var url = 'io/get_playerfields.php';

    new Ajax.Request(url, {
        method: 'get',
        parameters: details,
        onComplete: function(transport) {
            if (200 == transport.status) {
                var a = transport.responseText.evalJSON(true);
                
                $('person').update(a.prename +" "+ a.surname +", "+ a.location+ " (Runde "+a.round+")");
                for(var i=0; i<a.fields.length; i++) {
                    $('fieldnr'+a.fields[i]).setStyle({backgroundColor: '#00E500'});
                }
            }
            else {
                alert("Fehler bei der Abfrage: " + transport.status);
            }
        }
    });

}