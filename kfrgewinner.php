<html>
<head>
<title>Kuhfladenroulette Siegeransicht</title>

<script type="text/javascript" src="js/scriptaculous-js-1.9.0/lib/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous-js-1.9.0/src/scriptaculous.js"></script>
<script type="text/javascript" src="js/instructor.js"></script>
<script type="text/javascript">
    setInterval(function(){
        var gameInstruction = Object.clone(globalGameInstruction);
        console.log(gameInstruction);
        // window.location.reload(true);
    }, 1000);
</script>

<style type="text/css">
    body {  
        color:black;
        /* Gewinner alle 3 Minuten: */
        font-size:140%;
        /* Gewinner alle 5 Minuten: */
        /*font-size:200%;*/
        font-family:Helvetica;
        text-align: center;
        vertical-align: middle;
    }
    
    html {
        display: table;
        margin: auto;
    }

    #round {
        color: #E50000;
        font-size:120%;
        font-weight:bold;
    }

    #winnerhead {
        font-size:90%;
        font-style:italic;
        font-weight:bold;
    }

    #winnerlist {
        font-size:75%;
    }

    html {
        background: url(img/background.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
</head>
<body>
<img src="img/logoschrift.png" height="90px" border="0"></br>
<?php
    include_once 'util/cowpat.php';

    $current_time = cowpat_getCurrentTime();

    $round = 0;
    if (isset($_GET['round']) && $_GET['round']) {
        $round = (int)$_GET['round'];
    } else {
        $error = cowpat_getInstructions($instructions);
        if (strlen($error) == 0
                && array_key_exists("round", $instructions)
                && $instructions['round'])
            $round = (int)$instructions['round'];
    }

    for ($i = 1; $i < 3; $i++) {
        if ($round > 0 && $i != $round)
            continue;
    
        $error = cowpat_getState($state, $i);
        if (strlen($error) > 0)
            continue;

        if (!array_key_exists("starttime", $state)
                || !array_key_exists("maxtime", $state)
                || !array_key_exists("endtime", $state)
                || !array_key_exists("field", $state)
                || !array_key_exists("oldfield", $state))
            continue;

        $starttime = (int)$state['starttime'];
        $maxtime = (int)$state['maxtime'];
        $endtime = (int)$state['endtime'];
        if ($endtime == 0)
            $endtime = PHP_INT_MAX;
 
        echo "<span id=\"round\">Runde " . $i
            . "</span><br />" . PHP_EOL;

        $field = 0;
        $gametime = 0;
        $gametimeStep = 2 * 60 * 1000; // 2 minute step
        // $gametimeStep = 3 * 60 * 1000; // 3 minute step
        $gametime = $gametimeStep;
        $time = 0;
        $error = cowpat_getTimeFromGameTime($time, $i, $gametime);
        while (strlen($error) == 0
                && $starttime > 0
                && $time < $endtime
                && $time < $current_time
                && $gametime <= $maxtime) {
            $error = cowpat_getField($field, $i, $time);
            if (strlen($error) > 0)
                continue;
            else if ($field == 0)
            {
                $time = $current_time;
                continue;
            }

            $ranking = array();
            $error = cowpat_getRankingOnField($ranking, $i, $field, $time);
            if (strlen($error) > 0)
                continue;

            $gametimeMinutes = $gametime / 60000;
            echo "<br /><span id=\"winnerhead\">Sieger nach " . $gametimeMinutes .
                " Minuten (Feld " . $field . "): </span> <br />" . PHP_EOL
                . "<span id=\"winnerlist\">";
            for ($j = 0; $j < 3 && $j < count($ranking); $j++) {
                $player = $ranking[$j];
                //echo "#" . ($j+1) . ": " . $player['prename'] . " " .
                echo $player['prename'] . " " .
                    $player['surname'] . ", " . $player['location'] .
                    " (Los " . $player['id'] . ")<br />" . PHP_EOL;
            }
            echo "</span>" . PHP_EOL;

            $gametime += $gametimeStep;
            $error = cowpat_getTimeFromGameTime($time, $i, $gametime);
        }
        if (strlen($error) > 0)
            echo "<br /><b>" . $error . "</b><br />" . PHP_EOL;
        if ((int)$state['endtime'] > 0) {
            $error = cowpat_getField($field, $i, $endtime);
            if (strlen($error) > 0)
                continue;

            // Check if game has finished in paused state
            if ($field == 0)
                $field = (int)$state['field'];
            if ($field == 0)
                $field = (int)$state['oldfield'];

            $ranking = array();
            $error = cowpat_getRankingOnField($ranking, $i, $field, $endtime);
            if (strlen($error) > 0)
                continue;

            echo "<br /><span id=\"winnerhead\">Gesamtsieger <i>(Feld "
                . $field . ")</i>: </span> <br />" . PHP_EOL
                . "<span id=\"winnerlist\">";
            for ($j = 0; $j < 3 && $j < count($ranking); $j++) {
                $player = $ranking[$j];
                //echo "#" . ($j+1) . ": " . $player['prename'] . " " .
                echo $player['prename'] . " " .
                    $player['surname'] . ", " . $player['location'] .
                    " (Los " . $player['id'] . ")<br />" . PHP_EOL;
            }
            echo "</span>" . PHP_EOL;
        }
        
        if (strlen($error) > 0) {
            echo "<br /><b>" . $error . "</b><br />" . PHP_EOL;
            $error = "";
        }
        
        echo "<br />";
    }
    
    if (strlen($error) > 0) {
        echo "<br /><b>" . $error . "</b><br />" . PHP_EOL;
    }
    
    echo '</body>' . PHP_EOL;
?>

</html>
