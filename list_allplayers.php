<?php
    include_once 'util/error.php';
    include_once 'util/db.php';
    $mode = isset($_GET['csv']) ? 'csv' : 'html';

    if( $mode=='html' ) {
        echo '
        <html>
            <head>
                <style type="text/css">
                    body {  color:black;
                            font-size:3pt;
                        }
                    table { 
                            empty-cells: show;
                            border-style: none;
                            border-collapse: collapse;
                        }
                    td {
                            border-width: 1px;
                            border-style: solid;
                            padding:1px;
                            font-size:10pt;
                        }
                    th {
                            border-width: 2px;
                            border-style: solid;
                            padding:1px;
                            font-size:10pt;
                        }
                </style>
                <title>Kuhfladenroulette Feldliste</title>
            </head>
            <body>';
    }

    $dbconn = db_connect();
    $result = db_query( $dbconn, "SELECT TICKETS.SURNAME, TICKETS.PRENAME, TICKETS.LOCATION, TICKETMAP.FIELD_ID, TICKETMAP.ROUND".
                        " FROM TICKETS, TICKETMAP".
                        " WHERE TICKETMAP.TICKET_ID=TICKETS.ID".
                        " ORDER BY TICKETS.SURNAME, TICKETS.PRENAME, TICKETS.LOCATION, TICKETMAP.ROUND, TICKETMAP.FIELD_ID");
        if(!$result) {
            echo db_getErrorMessage();
        } else {
            $prename="";
            $surname="";
            $location="";
            $field1="";
            $field2="";

            if($mode=='html') {
                echo '<table><tr><th>Nachname</th><th>Vorname</th><th>Ort</th><th>Felder (Runde1)</th><th>Felder (Runde2)</th></tr>';
            }
            while ($row = db_fetchRow($result)) {
                if( $prename!=$row[1] || $surname!=$row[0] || $location!=$row[2] ) {
                    if($prename!="" && $surname!="" && $location!="") {
                        if( $mode=='html' )
                            echo "<tr><td>$surname</td><td>$prename</td><td>$location</td><td>$field1</td><td>$field2</td></tr>\n";
                        else
                            echo "$surname;$prename;$location;$field1;$field2\n";
                    }
                    $surname=$row[0];
                    $prename=$row[1];
                    $location=$row[2];
                    $field1="";
                    $field2="";
                }
                
                //Felder sammeln
                if($row[4]==1) {
                    if($field1!="")
                        $field1.=",";
                    $field1.=$row[3];
                }
                else if($row[4]==2) {
                    if($field2!="")
                        $field2.=",";
                    $field2.=$row[3];
                }   
            }
            if($prename!="" && $surname!="" && $location!="") {
                if( $mode=='html' )
                    echo "<tr><td>$surname</td><td>$prename</td><td>$location</td><td>$field1</td><td>$field2</td></tr>\n";
                else
                    echo "$surname;$prename;$location;$field1;$field2\n";
            }
            if($mode=='html') {
                echo "</table>";
            }
        }
        db_disconnect($dbconn);

    if( $mode=='html' ) {
        echo '
                </body>
            </html>';
    }

?>


