<?php
    include_once 'util/error.php';
    include_once 'util/db.php';
    $mode = isset($_GET['csv']) ? 'csv' : 'html';

    if( $mode=='html' ) {
        echo '
        <html>
            <head>
                <style type="text/css">
                    body {  color:black;
                            font-size:3pt;
                        }
                    table { 
                            empty-cells: show;
                            border-style: none;
                            border-collapse: collapse;
                        }
                    td {
                            border-width: 1px;
                            border-style: solid;
                            padding:1px;
                            font-size:10pt;
                        }
                    th {
                            border-width: 2px;
                            border-style: solid;
                            padding:1px;
                            font-size:10pt;
                        }
                </style>
                <title>Kuhfladenroulette Tickets</title>
            </head>
            <body>';
    }

    $dbconn = db_connect();
    $result = db_query( $dbconn, "SELECT ID, PRENAME, SURNAME, LOCATION".
                        " FROM TICKETS ORDER BY ID ASC");
        if(!$result) {
            echo db_getErrorMessage();
        } else {
            $id="";
            $prename="";
            $surname="";
            $location="";

            if($mode=='html') {
                echo '<table><tr><th>ID</th><th>Vorname</th><th>Nachname</th><th>Ort</th></tr>';
            }
            while ($row = db_fetchRow($result)) {
                $id=$row[0];
                $prename=$row[1];
                $surname=$row[2];
                $location=$row[3];
                if( $mode=='html' )
                    echo "<tr><td>$id</td><td>$prename</td><td>$surname</td><td>$location</td></tr>\n";
                else
                    echo html_entity_decode("$id;$prename;$surname;$location\n", ENT_NOQUOTES, "UTF-8");
            }
            if($mode=='html') {
                echo "</table>";
            }
        }
        db_disconnect($dbconn);

    if( $mode=='html' ) {
        echo '
                </body>
            </html>';
    }

?>


