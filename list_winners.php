<?php
    include_once 'util/cowpat.php';

    $current_time = cowpat_getCurrentTime();

    echo "<html><body>" . PHP_EOL;
    for ($i = 1; $i < 3; $i++) {
        $error = cowpat_getState($state, $i);
        if (strlen($error) > 0)
            continue;

        $starttime = (int)$state['starttime'];
        $maxtime = (int)$state['maxtime'];
        $endtime = (int)$state['endtime'];
        if ($endtime == 0)
            $endtime = PHP_INT_MAX;
 
        echo "<b>Runde " . $i . "</b><br />" . PHP_EOL;

        $field = 0;
        $gametime = 0;
        $gametimeStep = 2 * 60 * 1000; // 2 minute step
        // $gametimeStep = 3 * 60 * 1000; // 3 minute step
        $gametime = $gametimeStep;
        $time = 0;
        $error = cowpat_getTimeFromGameTime($time, $i, $gametime);
        while (strlen($error) == 0
                && $starttime > 0
                && $time < $endtime
                && $time < $current_time
                && $gametime <= $maxtime) {
            $error = cowpat_getField($field, $i, $time);
            if (strlen($error) > 0)
                continue;
            else if ($field == 0)
            {
                $time = $current_time;
                continue;
            }

            $ranking = array();
            $error = cowpat_getRankingOnField($ranking, $i, $field, $time);
            if (strlen($error) > 0)
                continue;

            $gametimeMinutes = $gametime / 60000;
            echo "<br />Sieger nach " . $gametimeMinutes .
                " Minuten [Feld " . $field . "]: <br />" . PHP_EOL;
            for ($j = 0; $j < 3 && $j < count($ranking); $j++) {
                $player = $ranking[$j];
                echo $player['id'] . ";" . $player['prename'] . " " .
                    $player['surname'] . " (" . $player['location'] .
                    ")<br />" . PHP_EOL;
            }

            $gametime += $gametimeStep;
            $error = cowpat_getTimeFromGameTime($time, $i, $gametime);
        }
        if (strlen($error) > 0)
            echo "<br /><b>" . $error . "</b><br />" . PHP_EOL;
        if ((int)$state['endtime'] > 0) {
            $error = cowpat_getField($field, $i, $endtime);
            if (strlen($error) > 0)
                continue;

            // Check if game has finished in paused state
            if ($field == 0)
                $field = (int)$state['field'];
            if ($field == 0)
                $field = (int)$state['oldfield'];

            $ranking = array();
            $error = cowpat_getRankingOnField($ranking, $i, $field, $endtime);
            if (strlen($error) > 0)
                continue;

            echo "<br/>Gesamtsieger [Feld " . $field . "]: <br />" . PHP_EOL;
            for ($j = 0; $j < 3 && $j < count($ranking); $j++) {
                $player = $ranking[$j];
                echo $player['id'] . ";" . $player['prename'] . " " .
                    $player['surname'] . " (" . $player['location'] .
                    ")<br />" . PHP_EOL;
            }
        }
        
        if (strlen($error) > 0) {
            echo "<br /><b>" . $error . "</b><br />" . PHP_EOL;
            $error = "";
        }
        
        echo "<br />";
    }
    
    if (strlen($error) > 0) {
        echo "<br /><b>" . $error . "</b><br />" . PHP_EOL;
    }
    
    echo '</body> </html>' . PHP_EOL;
?>


