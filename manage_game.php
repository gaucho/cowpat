<?php
    // Get layout
	$layout = join("", file("html/layout.html"));

    $layout = str_replace(
        "<body>", '<body onload="createfieldtable()">', $layout);
    $layout = str_replace(
        "<!--javascript-->",
        '<script type="text/javascript" src="js/kfradminpanel.js"></script> ',
        $layout);

    // Get content
    $content = join("", file("html/manage_game.html"));

    // Enter content
    $layout = str_replace("{phpinput}", $content, $layout);

    // Print site
    print $layout
?>
