<?php
include_once 'util/error.php';

include_once 'util/cowpat.php';
include_once 'util/parse.php';

$status = "";
$managecontent = "";
$filtercode = "";
$show_tickets = TRUE;

///////////////////////////////////////////////////////////////////////////////
// Change ticket code

if ( isset($_POST['change_ticket']) ) {
    $ticket = $_POST['ticket'];
    $prename = htmlentities(trim($_POST['prename']), ENT_NOQUOTES, "UTF-8");
    $surname = htmlentities(trim($_POST['surname']), ENT_NOQUOTES, "UTF-8");
    $location = htmlentities(trim($_POST['location']), ENT_NOQUOTES, "UTF-8");

    // Check metadata
    if (strlen($prename) == 0) {
        $show_tickets = FALSE;
        $status = $status . "
<li>Kein Vorname angegeben</li>";
    }
    if (strlen($surname) == 0) {
        $show_tickets = FALSE;
        $status = $status . "
<li>Kein Nachname angegeben</li>";
    }
    if (strlen($location) == 0) {
        $show_tickets = FALSE;
        $status = $status . "
<li>Kein Ort angegeben</li>";
    }

    if (strlen($status) > 0) {
        $status = "<div class=\"error\">
<h3>Fehler bei der Los&auml;nderung:</h3>
<ul>
" . $status . "
</ul>
</div>";
    } else {
        $error = cowpat_updateTicket($ticket, $prename, $surname, $location);
        if (strlen($error) != 0) {
            $show_tickets = FALSE;
            $status = "<div class=\"error\">
<h3>Fehler bei der &Auml;nderung von Los " . $ticket . "</h3>
  " . $error . "
</div>";
        } else {
            $status .= "<div class=\"success\">
<h3>Los&auml;nderung erfolgreich</h3>
  Das Los " . $ticket . " wurden erfolgreich aktualisiert.
</div>";
        }
    }
} else if (isset($_POST['delete_ticket'])) {
    $ticket = $_POST['ticket'];
    $error = cowpat_removeTicket($ticket);
    if (strlen($error) > 0) {
        $status = "<div class=\"error\">
<h3>Fehler beim L&ouml;schen von Los " . $ticket ."</h3>
  " . $error . "
</div>";
    } else {
        $status .= "<div class=\"success\">
<h3>Los&auml;nderung erfolgreich</h3>
  Das Los " . $ticket . " wurden erfolgreich gel&ouml;scht!
</div>";
    }
}

///////////////////////////////////////////////////////////////////////////////
// Change ticket interface

if (isset($_GET['id']) or !$show_tickets) {
    $ticket_id = htmlentities($_GET['id']);
    if (!$show_tickets) {
        $ticket_id = $_POST['ticket'];
    }
    $ticketinfo = array();
    $error = cowpat_getTicket($ticketinfo, $ticket_id);
    if (strlen($error) > 0) {
        $status = "<div class=\"error\">
<h3>Fehler bei Losverwaltung</h3>
  Das Los '" . $ticket_id . "' konnte nicht gefunden werden!
</div>";
    } else {
        $managecontent = "
<form action=\"manage_tickets.php\" method=\"post\" enctype=\"multipart/form-data\">
<table><tr>
  <td id=\"bold\">Losnummer :</td>
  <td><input type=\"text\" name=\"ticket\" value=\"".
    html_entity_decode($ticket_id, ENT_NOQUOTES, "UTF-8") .
    "\" size=\"64\" readonly /></td>
</tr><tr>
  <td id=\"bold\">Vorname :</td>
  <td><input type=\"text\" name=\"prename\" value=\"" .
    html_entity_decode($ticketinfo['prename'], ENT_NOQUOTES, "UTF-8") .
    "\" size = \"64\"/></td>
</tr><tr>
  <td id=\"bold\">Nachname :</td>
  <td><input type=\"text\" name=\"surname\" value=\"" .
    html_entity_decode($ticketinfo['surname'], ENT_NOQUOTES, "UTF-8") .
    "\" size = \"64\"/></td>
</tr><tr>
  <td id=\"bold\">Ort :</td>
  <td><input type=\"text\" name=\"location\" value=\"" .
    html_entity_decode($ticketinfo['location'], ENT_NOQUOTES, "UTF-8") .
    "\" size = \"64\"/></td>
</tr></table>
<hr />
<p>
    <input type=\"submit\" name=\"change_ticket\" value=\"&Auml;ndern\" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type=\"submit\" name=\"delete_ticket\" value=\"L&ouml;schen\" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type=\"submit\" name=\"back\" value=\"Zur&uuml;ck zur &Uuml;bersicht\" />
</p>
</form>";
        $show_tickets = FALSE;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Ticket overview
if ($show_tickets) {
    if (isset($_POST['filter_id'])) {
        $filter_id = htmlentities($_POST['filter_id'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_id) > 0)
            $filtercode .= "&filter_id=" . urlencode($_POST['filter_id']);
    } else if (isset($_GET['filter_id'])) {
        $filter_id = urldecode($_GET['filter_id']);
        $filter_id = htmlentities($filter_id, ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_id) > 0)
            $filtercode .= "&filter_id=" . $_GET['filter_id'];
    } else {
        $filter_id = "";
    }
    if (isset($_POST['filter_prename'])) {
        $filter_prename = htmlentities(
            $_POST['filter_prename'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_prename) > 0)
            $filtercode .= "&filter_prename=" .
                urlencode($_POST['filter_prename']);
    } else if (isset($_GET['filter_prename'])) {
        $filter_prename = urldecode($_GET['filter_prename']);
        $filter_prename = htmlentities(
            $_GET['filter_prename'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_prename) > 0)
            $filtercode .= "&filter_prename=" . $_GET['filter_prename'];
    } else {
        $filter_prename = "";
    }
    if (isset($_POST['filter_surname'])) {
        $filter_surname = htmlentities(
            $_POST['filter_surname'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_surname) > 0)
            $filtercode .= "&filter_surname=" .
                urlencode($_POST['filter_surname']);
    } else if (isset($_GET['filter_surname'])) {
        $filter_surname = urldecode($_GET['filter_surname']);
        $filter_surname = htmlentities(
            $_GET['filter_surname'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_surname) > 0)
            $filtercode .= "&filter_surname=" . $_GET['filter_surname'];
    } else {
        $filter_surname = "";
    }
    if (isset($_POST['filter_location'])) {
        $filter_location = htmlentities(
            $_POST['filter_location'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_location) > 0)
            $filtercode .= "&filter_location=" .
                urlencode($_POST['filter_location']);
    } else if (isset($_GET['filter_location'])) {
        $filter_location = urldecode($_GET['filter_location']);
        $filter_location = htmlentities(
            $_GET['filter_location'], ENT_NOQUOTES, "UTF-8");
        if (strlen($filter_location) > 0)
            $filtercode .= "&filter_location=" . $_GET['filter_location'];
    } else {
        $filter_location = "";
    }

    $tickets = array();
    $error = cowpat_getTickets($tickets,
        $filter_id, $filter_prename, $filter_surname, $filter_location);
    if (strlen($error) > 0) {
        $status = $status . "<div class=\"error\">
<h3>Fehler beim Abrufen der Tickets</h3>
 " . $error . "
</div>";
    } else {
        $i = 1;
        $ticketcount = sizeof($tickets);
        foreach($tickets as $id => $info) {
            $managecontent .= "
<tr>
    <td><a href=\"?id=" . $id . "\">" . $id . "</a></th>
    <td>" . $info['prename'] . "</th>
    <td>" . $info['surname'] . "</th>
    <td>" . $info['location'] . "</th>
</tr>";
            $i += 1;
        }

        $managecontent = "
<h3>Suchen:</h3>
<form id=\"filter_form\" action=\"manage_tickets.php\"
      method=\"post\" enctype=\"multipart/form-data\">
<table class=\"center\"><tr>
  <td id=\"bold\">Losnummer :</td>
  <td><input type=\"text\" name=\"filter_id\" value=\"". $filter_id .
    "\" size=\"64\"/></td>
</tr><tr>
  <td id=\"bold\">Vorname :</td>
  <td><input type=\"text\" name=\"filter_prename\" value=\"" . $filter_prename .
    "\" size = \"64\"/></td>
</tr><tr>
  <td id=\"bold\">Nachname :</td>
  <td><input type=\"text\" name=\"filter_surname\" value=\"" . $filter_surname .
    "\" size = \"64\"/></td>
</tr><tr>
  <td id=\"bold\">Ort :</td>
  <td><input type=\"text\" name=\"filter_location\" value=\"" . $filter_location .
    "\" size = \"64\"/></td>
</tr><tr>
  <td colspan=\"2\">
    <input type=\"submit\" name=\"filter_tickets\" value=\"Suchen\" />
  </td>
</tr></table>

<hr />
<h3>" . $ticketcount . " Lose:</h3>
<table class=\"center\">
<tr>
    <th>Losnummer</th>
    <th>Vorname</th>
    <th>Nachname</th>
    <th>Ort</th>
</tr>" .$managecontent . "
<tr><th colspan=\"4\">
</th></tr>
</table>
</div>";
    }
}

///////////////////////////////////////////////////////////////////////////////
// Print output

// Get layout
$layout = join("", file("html/layout.html"));
$content = join("", file("html/manage_tickets.html"));

// Enter content
$layout = str_replace("{phpinput}", $content, $layout);
$layout = str_replace("{phpstatus}", $status, $layout);
$layout = str_replace("{phpmanage}", $managecontent, $layout);

// Print site
print $layout;
?>
