<?php
include_once 'util/error.php';
include_once 'util/cowpat.php';

$round = 1;
$error = cowpat_getNextRound($round);
if (strlen($error) > 0) {
    print $error;
} else {
    $error = cowpat_initRound($round, 192);
    if (strlen($error) > 0) {
        print $error;
    } else {
        $error = cowpat_startRound($round);
        print $error;
    }
}
?>
