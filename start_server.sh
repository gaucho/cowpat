#!/bin/sh

if [ "$1" == "nginx" ]; then
   su root -c "
systemctl start mysqld
systemctl start php-fpm
systemctl start nginx
"
else
   su root -c "
systemctl start mysqld
systemctl start httpd
"
fi

