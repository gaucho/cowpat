<?php
    include '../util/error.php';
    include '../util/db.php';

    // Settings
    $fieldstart = 1;    // field where cow starts
    $fieldcount = 192;  // overall amount of fields

    db_connect();

    // Get round number (start with round 1 if COWPAT.INFO is empty)
    $result = db_query("SELECT MAX(ROUND) FROM COWPAT.INFO");
    if (!$result) {
        echo db_getErrorMessage();
        $round = -1;
    } else if (db_getRowCount($result) == 1) {
        $row = db_fetchRow($result);
        $round = (int)($row[0]) + 1;
    } else {
        $round = 1;
    }

    // Write info values to COWPAT.INFO
    if ($round > 0) {
        $sqlField = "INSERT INTO COWPAT.INFO VALUES (" .
            $round . ", 'field', " . $fieldstart . ")";
        $sqlRunning = "INSERT INTO COWPAT.INFO VALUES (" .
            $round . ", 'running', 1)";
        if ( !db_query($sqlField) || !db_query($sqlRunning) ) {
            echo db_getErrorMessage();
            $round = -1;
        }
    }

    // Truncate TICKETMAP table
    if ($round > 0) {
        if ( !db_query("TRUNCATE TABLE COWPAT.TICKETMAP") ) {
            echo db_getErrorMessage();
            $round = -1;
        }
    }

    // Fill TICKETMAP table
    if ($round > 0) {
        $fieldarray = array();
        for ( $i = 0; $i < $fieldcount; $i += 1 ) {
            $value = $i + 1;
            $fieldarray[$value] = $value;
        }

        // Get all tickets and assign them to fields
        $ticketResult = db_query("SELECT ID, PRENAME, SURNAME, LOCATION " . 
            "FROM COWPAT.TICKETS ORDER BY PRENAME, SURNAME, LOCATION");
        if ($ticketResult) {
            while($ticketRow = db_fetchRow($ticketResult)) {
                $ticket = $ticketRow[0];
                $prename = $ticketRow[1];
                $surname = $ticketRow[2];
                $location = $ticketRow[3];

                $ticketfieldarray = $fieldarray;

                // Get fields where player is already assigned
                $sql = "SELECT tm.FIELD_ID FROM COWPAT.TICKETMAP AS tm ".
                    "JOIN COWPAT.TICKETS AS t ON tm.TICKET_ID = t.ID " .
                    "WHERE t.PRENAME='" . $prename . 
                        "' AND t.SURNAME='" . $surname .
                        "' AND t.LOCATION='" . $location . "'";
                $assignedResult = db_query($sql);
                if (!$assignedResult) {
                    echo db_getErrorMessage();
                    break;
                }
                
                // Remove them from field pool
                while($assignedRow = db_fetchRow($assignedResult)) {
                    $field = (int)$assignedRow[0];
                    unset ($ticketfieldarray[$field]);
                }
                if (empty($ticketfieldarray)) {
                    // Wow, someone bought more tickets than fields are
                    // available. No fields will be excluded for selection.
                    // TODO: Assert $prename=="Matthias" && $surname=="Braun"
                    $ticketfieldarray = $fieldarray;
                }

                // Get min/max count of field reservations
                $minMaxSql = "SELECT MIN(s.fieldcount), MAX(s.fieldcount) " .
                    "FROM (" .
                        "SELECT FIELD_ID, COUNT(*) AS fieldcount " .
                        "FROM COWPAT.TICKETMAP GROUP BY FIELD_ID) AS s";
                $minMaxResult = db_query($minMaxSql);
                if ( !$minMaxResult ) {
                    echo db_getErrorMessage();
                    break;
                } else if (db_getRowCount($minMaxResult) == 1) {
                    $minMaxRow = db_fetchRow($minMaxResult);
                    $minRow = (int)$minMaxRow[0];
                    $maxRow = (int)$minMaxRow[1];
                } else {
                    $minRow = 0;
                    $maxRow = 0;
                }

                // Only assign fields which have not the max amount of players
                if ($minRow < $maxRow) {
                    $maxResult = db_query("SELECT f.fieldid FROM ( " .
                        "SELECT FIELD_ID AS fieldid, COUNT(*) AS fieldcount " .
                        "FROM COWPAT.TICKETMAP GROUP BY FIELD_ID) AS f " .
                        "WHERE f.fieldcount = " . $maxRow);
                    if (!$maxResult) {
                        echo db_getErrorMessage();
                        break;
                    }
                    while ($maxResultRow = db_fetchRow($maxResult)) {
                        $field = (int)$maxResultRow[0];
                        if (count($ticketfieldarray) > 1) {
                            unset($ticketfieldarray[$field]);
                        }
                    }
                }

                // Set field
                $field = array_rand($ticketfieldarray);
                $sqlInsert = "INSERT INTO COWPAT.TICKETMAP VALUES (".
                    $ticket . ", " . $field . ")";
                if ( ! db_query($sqlInsert) ) {
                    echo db_getErrorMessage();
                    break;
                }
            } // while ($ticketRow)
        } // if ($ticketResult)
    }

    db_disconnect();

    echo 'start_game.php succeeded!'
?>
