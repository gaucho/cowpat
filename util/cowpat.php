<?php
/**
 * @file cowpat.php
 * Functions to manage the cowpat game.
 */

include_once 'db.php';

/**
 * Add a ticket to database.
 * @param[in] dbconn database connection
 * @param[in] prename prename of ticket owner
 * @param[in] surname surname of ticket owner
 * @param[in] location location of ticket owner
 * @param[in] tickets ticket numbers
 * @return empty string on success, else error message
 */
function cowpat_addTickets($dbconn, $prename, $surname, $location, $tickets) {
    $ticketcount = 0;
    $sql = "INSERT INTO COWPAT.TICKETS VALUES";
    foreach($tickets as $ticket) {
        if ($ticketcount > 0) {
            $sql .= ",";
        }
        $sql .= "(" . $ticket . ", '" . $prename . "', '" .
            $surname . "', '" . $location . "')";
        ++$ticketcount;
    } 
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        return "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    // Map tickets for all open rounds
    $rounds = array();
    $error = cowpat_getInitializedRounds($dbconn, $rounds);
    if (empty($error)) {
        foreach($rounds as $round) {
            $roundinfo = array();
            $error = cowpat_getRoundState($dbconn, $round, $roundinfo);
            if (empty($error))
                $error = cowpat_mapTickets($dbconn,
                    $prename, $surname, $location, $tickets, $roundinfo);
            if (!empty($error)) {
                break;
            }
        } // foreach($rounds as $round)
    }

    return $error;
}

/**
 * Clear tickets and fieldlog.
 * @param[in] id ticket number
 * @return empty string on success, else error message
 */
function cowpat_clear($id) {
    $error = "";

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "DELETE FROM COWPAT.FIELDLOG";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
        db_disconnect($dbconn);
        return $error;
    }

    $sql =  "UPDATE COWPAT.INFO SET VALUE=0 ".
        "WHERE NAME IN ('starttime', 'endtime')";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    # Delete ticket map
    $sql = "DELETE FROM COWPAT.TICKETMAP";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    # Delete tickets
    $sql = "DELETE FROM COWPAT.TICKETS";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    db_disconnect($dbconn);
    return $error;
}

/**
 * Continue a stopped round.
 * @param[in] round game round which should be continue
 * @return empty string on success, else error message
 */
function cowpat_continueRound($round) {
    // Get round
    if (!is_numeric($round)) {
        return "Cannot continue round: parameter is not numeric";
    }
    
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    // Set stop time
    $error = "";
    $sqlFieldTime = "UPDATE COWPAT.INFO SET VALUE=0" . 
        " WHERE ROUND=" . $round . " AND NAME='endtime'";
    if (!db_query($dbconn, $sqlFieldTime)) {
        $error = db_getErrorMessage($dbconn);
    }        

    db_disconnect($dbconn);
    return $error;
}

/**
 * Get current time.
 * @return current time in ms
 */
function cowpat_getCurrentTime() {
    return (int)(floor(microtime(true)*1000));
}

/**
 * Get the current field for the round.
 * @param[out] field current field number
 * @param round game round
 * @param time time the field should be retrieved for
 * @retun non-empty string on error
 */
function cowpat_getField(&$field, $round, $time = FALSE) {
    $state = array();
    $error = cowpat_getState($state, $round);
    if (strlen($error) > 0) {
        return $error;
    }
    $starttime = NULL;
    if (array_key_exists("starttime", $state)
            && is_numeric($state['starttime'])) {
        $starttime = (int)$state['starttime'];
    }

    // If time is FALSE or no start time is available get field from state
    if ($time === FALSE || $starttime == NULL) {
        if (array_key_exists("field", $state)
                && is_numeric($state['field'])) {
            $field = (int)$state['field'];
        } else {
            $error = "Could not retrieve field for round " . $round;
        }
        return $error;
    }

    // Get field from field log
    if ($time < $starttime) {
        $gametime = $time;
        $error = cowpat_getTimeFromGameTime($time, $round, $gametime);
        if (strlen($error) > 0)
            return $error;
    }

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT FIELD_ID FROM COWPAT.FIELDLOG " .
        " WHERE ROUND = " . $round .
        " AND FIELD_ENTERED < " . $time .
        " AND (FIELD_LEFT IS NULL OR FIELD_LEFT >= " . $time . ")";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        $row = db_fetchRow($result);
        $field = (int) $row[0];
    }
    db_disconnect($dbconn);
    
    return $error;
}

/**
 * Get the current round time.
 * @param[out] timeInField time spent in field
 * @param[in] round round number
 * @param[in] field field id or FALSE to get current field for round
 * @return empty string on success, else error message
 */
function cowpat_getGameTime(&$timeOfRound, $round, $time = FALSE) {
    if ($time === FALSE) {
        $time = cowpat_getCurrentTime();
    }

    // Update time according to start, end and maximum time
    $state = array();
    $error = cowpat_getState($state, $round);
    if (strlen($error) > 0) {
        return $error;
    }

    $starttime = (int)$state['starttime'];
    $endtime = (int)$state['endtime'];
    $maxtime = (int)$state['maxtime'];

    if ( $starttime == NULL ) {
        $timeOfRound = 0; // Round not started
        return "";
    } else if ( $starttime > $time ) {
        $gametime = $time;
        $error = cowpat_getTimeFromGameTime($time, $round, $gametime);
        if (strlen($error) > 0)
            return $error;
    }

    $pausetime = 0;
    $error = cowpat_getTimeOnField($pausetime, $round, 0, $time);
    if (strlen($error) > 0)
        return $error;

    if ($time > ($starttime + $maxtime + $pausetime))
        $time = ($starttime + $maxtime + $pausetime);
    if ( $endtime > 0 && $time > $endtime)
        $time = $endtime;

    $timeOfRound = $time - $starttime - $pausetime;
    return "";
}

/**
 * Get an array with all initialized but not started rounds.
 * @param[in] dbconn database connection
 * @param[out] roundarray array with round values
 */
function cowpat_getInitializedRounds($dbconn, &$roundarray) {
    $roundarray = array();

    $sql = "SELECT ROUND FROM COWPAT.INFO WHERE NAME='starttime' AND " .
        "VALUE=0";
    $result = db_query($dbconn, $sql);
    if(!$result) {
        $db_error = db_getErrorMessage($dbconn);
        return "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    while ($row = db_fetchRow($result)) {
        $roundarray[] = (int)$row[0];
    };
    return "";
}

/**
 * Get the game status.
 * @param[out] instructionarray key/value array with instruction variables
 * @return empty string on success, else error message
 */
function cowpat_getInstructions(&$instructionarray) {
    $error = "";
    $instructionarray = array();
    
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT NAME, VALUE FROM COWPAT.INSTRUCTIONS";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        while ($row = db_fetchRow($result)) {
            $instructionarray[$row[0]] = $row[1];
        }
    }
    
    db_disconnect($dbconn);
    return $error;
}

/**
 * Get the next round value.
 * @param[out] round next game round
 * @return empty string on success, else error message
 */
function cowpat_getNextRound(&$round) {
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT MAX(ROUND) FROM COWPAT.INFO";
    $result = db_query($dbconn, $sql);
    if(!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
        return $error;
    }
    $rowcount = db_getRowCount($result);
    if ($rowcount === 0) {
        $round = 1;
    } else {
        $row = db_fetchRow($result);
        $round = ((int)$row[0]) + 1;
    }
    
    db_disconnect($dbconn);
    return "";
}

/**
 * Get the players which are currently on the defined field.
 * Pass field = FALSE to use current cow field from status.
 * @param[out] playerarray players mapped by id to {prename, surname, location}
 * @param[in] round game round (default current round from cowpat_getRound)
 * @param[in] field where players should be retrieved (default = cow field)
 * @return empty string on success, else error message
 */
function cowpat_getPlayersOnField(&$playerarray, $round, $field = FALSE) {
    $playerarray = array();
    $error = "";
    
    // Get field
    if ($field === FALSE) {
        $error = cowpat_getField($field, $round);
        if (strlen($error) > 0)
            return "Cannot get players for field '" . $field . 
                "' and round '" . $round . "': No information found.";
    }

    // Get players
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql =  "SELECT tm.TICKET_ID, t.PRENAME, t.SURNAME, t.LOCATION" .
            " FROM COWPAT.TICKETMAP AS tm ".
            " JOIN COWPAT.TICKETS AS t ON tm.TICKET_ID=t.ID".
            " WHERE tm.ROUND = " . $round . " AND tm.FIELD_ID = " . $field .
            " ORDER BY tm.TICKET_ID " . ($round % 2 == 1 ? "ASC" : "DESC");
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        $rowindex = 0;
        while ($row = db_fetchRow($result)) {
            // Randomize player array
            $rowoffset = $row[0] % ($rowindex + 1);
            //$rowoffset = $rowindex;
            if ($rowoffset == $rowindex) {
                array_push($playerarray, array( 'id'        => $row[0],
                                                'prename'   => $row[1],
                                                'surname'   => $row[2],
                                                'location'  => $row[3]  ));
            } else {
                array_splice($playerarray, $rowoffset, 0, array(array(
                                                'id'        => $row[0],
                                                'prename'   => $row[1],
                                                'surname'   => $row[2],
                                                'location'  => $row[3]  )));
            }
            $rowindex++;
        }
    }
    
    db_disconnect($dbconn);
    return $error;
}

/**
 * Get the player ranking for the defined field.
 * Pass field = FALSE to use current cow field from status.
 * @param[out] playerarray players mapped by id to {prename, surname, location}
 * @param[in] round game round (default current round from cowpat_getRound)
 * @param[in] field where players should be retrieved (default = cow field)
 * @return empty string on success, else error message
 */
function cowpat_getRankingOnField(&$playerarray, $round, $field = FALSE, $time = FALSE) {
    $error = "";

    // Get current state
    $state = array();
    $error = cowpat_getState($state, $round);
    if (strlen($error) > 0) {
        return $error;
    } else if ( $field == FALSE ) {
        $field = $state["field"];
        if (is_numeric($field) == FALSE) {
            return "Failed to get current field from state";
        }
    }

    // Get players on current field
    $error = cowpat_getPlayersOnField($playerarray, $round, $field);
    if (strlen($error) > 0) {
        return $error;
    }

    // Get time spent on field
    if ($time === FALSE)
        $time = cowpat_getCurrentTime();
    $timeInField = 0;
    $error = cowpat_getTimeOnField($timeInField, $round, $field, $time);
    if (strlen($error) > 0) {
        return $error;
    }

    // Reorder playerarray using fieldperiod and winnercount from state
    $fieldperiod = $state["fieldperiod"];
    $winnercount = $state["winnercount"];
    if (is_numeric($field) == FALSE) {
        return "Failed to get fieldperiod from state";
    } else if (is_numeric($winnercount) == FALSE) {
        return "Failed to get fieldperiod from state";
    }

    if (!empty($playerarray)) {
        $playercount = count($playerarray);
        $winnerindex = (int)((int)$timeInField / (int)$fieldperiod);
        $winnerindex = $winnerindex * (int)$winnercount;
        $winnerindex = $winnerindex % count($playerarray);
        if ($winnerindex > 0) {
            $playerarray = array_merge(array_slice($playerarray, $winnerindex),
                array_slice($playerarray, 0, $winnerindex));
        }
    }

    return $error;
}

/**
 * Get the status of a round.
 * @param[in] dbconn database connection
 * @param[in] round game round for which state is retrieved (default = current)
 * @param[out] statearray key/value array with state variables
 * @return empty string on success, else error message
 */
function cowpat_getRoundState($dbconn, $round, &$statearray) {
    $error = "";
    $statearray = array();

    if (!is_numeric($round)) {
        return "Cannot get state: round parameter not numeric";
    }

    $sql = "SELECT NAME, VALUE FROM COWPAT.INFO WHERE ROUND = " . $round;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        while ($row = db_fetchRow($result)) {
            $statearray[$row[0]] = (int)$row[1];
        }
	/*$statearray['id'] = $round;*/
    }

    return $error;
}

/**
 * Get an array with all known rounds.
 * @param[out] roundarray array with round values
 */
function cowpat_getRounds(&$roundarray) {
    $roundarray = array();
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT DISTINCT ROUND FROM COWPAT.INFO";
    $result = db_query($dbconn, $sql);
    if(!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
        return $error;
    } else {
        while ($row = db_fetchRow($result)) {
            $roundarray[] = (int)$row[0];
        };
    }
    
    db_disconnect($dbconn);
    return "";
}

/**
 * Get the game status.
 * @param[out] statearray key/value array with state variables
 * @param[in] round game round for which state is retrieved
 * @return empty string on success, else error message
 */
function cowpat_getState(&$statearray, $round) {
    if (!is_numeric($round)) {
        return "Cannot get state: round parameter not numeric";
    }
    
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $error = cowpat_getRoundState($dbconn, $round, $statearray);

    db_disconnect($dbconn);
    return $error;
}

/**
 * Get the round states.
 * @param[out] statearray key/value array with state variables
 * @param[in] round game round for which state is retrieved (default = current)
 * @return empty string on success, else error message
 */
function cowpat_getStates(&$statearray) {
    $error = "";
    $statearray = array();
    
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT ROUND, NAME, VALUE FROM COWPAT.INFO ORDER BY ROUND ASC";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        $round = -1;
        $roundarray = array();
        while ($row = db_fetchRow($result)) {
            $old_round = $round;
            $round = (int)$row[0];
            if ($old_round < $round) {
                $roundarray = array();
                $statearray[$round] = $roundarray;
            }
            $roundarray[$row[1]] = (int)$row[2];
        }
    }
    
    db_disconnect($dbconn);
    return $error;
}

/**
 * Get ticket info array for a defined ticket number.
 * @param[out] infoarray array containing key value pairs of ticket
 * @param[in] ticketnr ticket which is looked up
 * @return empty string on success, else error message
 */
function cowpat_getTicket(&$infoarray, $ticketnr) {
    $error = "";
    $infoarray = array();

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT PRENAME, SURNAME, LOCATION FROM COWPAT.TICKETS " .
        "WHERE ID = " . $ticketnr;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else if ($row = db_fetchRow($result)) {
        $infoarray = array( 'prename'   => $row[0],
                            'surname'   => $row[1],
                            'location'  => $row[2]  );
    }
    
    db_disconnect($dbconn);
    return $error;
}

/**
 * Get tickets with info on name and location. You may define filter values
 * to filter the resulting array.
 * @param[out] ticketarray array containing all tickets which match the filter
 * @param[in] filter_id ticket id
 * @param[in] filter_prename ticket owner prename
 * @param[in] filter_surname ticket owner surname
 * @param[in] filter_location ticket owner location
 * @return empty string on success, else error message
 */
function cowpat_getTickets(&$ticketarray,
        $filter_id = "", $filter_prename = "",
        $filter_surname = "", $filter_location = "") {
    $error = "";
    $ticketarray = array();

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "SELECT ID, PRENAME, SURNAME, LOCATION FROM COWPAT.TICKETS";
    $sql_filter = "";

    if (strlen($filter_id) > 0) {
        $sql_filter = " WHERE ID = " . $filter_id;
    }
    if (strlen($filter_prename) > 0) {
        if (strlen($sql_filter) === 0) {
            $sql_filter = " WHERE ";
        } else {
            $sql_filter .= "AND ";
        }
        $sql_filter .= "PRENAME LIKE '%" . $filter_prename . "%'";
    }
    if (strlen($filter_surname) > 0) {
        if (strlen($sql_filter) === 0) {
            $sql_filter = " WHERE ";
        } else {
            $sql_filter .= "AND ";
        }
        $sql_filter .= "SURNAME LIKE '%" . $filter_surname . "%'";
    }
    if (strlen($filter_location) > 0) {
        if (strlen($sql_filter) === 0) {
            $sql_filter = " WHERE ";
        } else {
            $sql_filter .= "AND ";
        }
        $sql_filter .= "LOCATION LIKE '%" . $filter_location . "%'";
    }
    $sql = $sql . $sql_filter . " ORDER BY ID ASC";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        while ($row = db_fetchRow($result)) {
            $ticketarray[$row[0]] =  array( 'prename'   => $row[1],
                                            'surname'   => $row[2],
                                            'location'  => $row[3]  );
        }
    }
    
    db_disconnect($dbconn);
    return $error;
}

/**
 * Get the amount of time which was spent in defined field.
 * @param[out] timeInField time spent in field
 * @param[in] round round number
 * @param[in] field field id or FALSE to get current field for round
 * @param[in] time maximum time (default: current time)
 * @return empty string on success, else error message
 */
function cowpat_getTimeOnField(&$timeInField, $round, $field = FALSE, $time = FALSE) {
    $error = "";
    if ($field === FALSE) {
        $error = cowpat_getField($field, $round, $time);
        if (strlen($error) > 0) {
            return $error;
        }
    }
    if ($time === FALSE) {
        $time = cowpat_getCurrentTime();
    }

    // Update time according to start, end and maximum time
    $state = array();
    $error = cowpat_getState($state, $round);
    if (strlen($error) > 0) {
        return $error;
    }

    $starttime = (int)$state['starttime'];
    $endtime = (int)$state['endtime'];
    $maxtime = (int)$state['maxtime'];

    if ( $starttime == NULL ) {
        $timeOnField = 0; // Round not started
        return "";
    } else if ( $starttime > $time ) {
        $gametime = $time;
        $error = cowpat_getTimeFromGameTime($time, $round, $gametime);
        if (strlen($error) > 0)
            return $error;
    }
    if ( $endtime > 0 && $time > $endtime )
        $time = $endtime;

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $gametime = 0;
    $timeInField = 0;
    $sql = "SELECT FIELD_ENTERED, FIELD_LEFT, FIELD_ID" .
        " FROM COWPAT.FIELDLOG WHERE ROUND=". $round .
        " AND FIELD_ENTERED < " . $time .
        " ORDER BY FIELD_ENTERED ASC";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        while (($row = db_fetchRow($result))
                && $gametime < $maxtime) {
            $entered = (int)$row[0];
            $left = (int)$row[1];
            $field_id = (int) $row[2];

            // Set left to $time if $left is not set
            if ($left == NULL || $left > $time) {
                $left = $time;
            }

            // Get duration spent in field
            $duration = $left - $entered;

            // Increase gametime and check if maxtime is reached
            if ($field_id != 0) {
                $gametime += $duration;
                if ($gametime > $maxtime) {
                    $duration -= ($gametime - $maxtime);
                    $gametime = $maxtime;
                }                
            }

            // Increase timeInField
            if ($field == $field_id) {
                $timeInField += $duration;
            }
        }
    }

    db_disconnect($dbconn);
    return $error;
}

/**
 * Get a real timestamp for the game time.
 * @param[out] time timestamp
 * @param[in] round round number
 * @param[in] gametime game time
 * @return empty string on success, else error message
 */
function cowpat_getTimeFromGameTime(&$time, $round, $gametime) {
    if (!is_numeric($round) || !is_numeric($gametime)) {
        return "Could not get time from game time: Non numeric parameter.";
    }

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $error = "";
    $duration = (int)$gametime;
    $sql = "SELECT FIELD_ENTERED, FIELD_LEFT, FIELD_ID" .
        " FROM COWPAT.FIELDLOG WHERE ROUND=". $round .
        " ORDER BY FIELD_ENTERED ASC";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    } else {
        while ($row = db_fetchRow($result)) {
            $entered = (int)$row[0];
            $left = (int)$row[1];
            if ($row[2] == 0)
                continue; // Skip pause

            $time = $entered + $duration;
            if ($left == NULL || $left > $time) {
                db_disconnect($dbconn);
                return "";
            }

            $fieldduration = ($left - $entered);
            if ($fieldduration > $duration) {
                db_disconnect($dbconn);
                return "";
            }
            
            $duration -= $fieldduration;
        }
    }

    db_disconnect($dbconn);
    return $error;
}

/**
 * Initialize a round.
 * @param[in] round round number
 * @param[in] state initial state array
 * @param[in] fieldperiod winning player interval period on field
 * @param[in] winnercount winning player count
 */
function cowpat_initRound($round, $state) {
    $time = microtime(TRUE);

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $oldState = array();
    $error = cowpat_getRoundState($dbconn, $round, $oldState);
    if (!empty($error))
        return $error;
    else if (!empty($oldState))
        return "Round " . $round . " can't be initialized: " .
            "Round already exists!";

    $state["id"] = $round;
    $state["starttime"] = 0;
    $state['endtime'] = 0;

    // Write default values to array if they are not set
    if (!array_key_exists("field", $state))
        $state["field"] = 0;
    if (!array_key_exists("oldfield", $state))
        $state["oldfield"] = 0;
    if (!array_key_exists("fieldcount", $state))
        $state["fieldcount"] = 192;
    if (!array_key_exists("fieldperiod", $state))
        $state["fieldperiod"] = 10000;
    if (!array_key_exists("maxtime", $state))
        $state["maxtime"] = 10 * 60 * 1000; // 2019-08-26 15 * 60 * 1000;
    if (!array_key_exists("winnercount", $state))
        $state["winnercount"] = 3;

    // Get all tickets and assign them to fields
    $ticketSql = "SELECT ID, PRENAME, SURNAME, LOCATION " . 
            "FROM COWPAT.TICKETS ORDER BY SURNAME, PRENAME, LOCATION";
    $ticketResult = db_query($dbconn, $ticketSql);
    if (!$ticketResult) {
        $error = db_getErrorMessage($dbconn);
        db_disconnect($dbconn);
        return $error;
    }

    // Clear Ticketmap to make sure, we don't have duplicates
    $deleteResult = db_query(
        $dbconn, "DELETE FROM COWPAT.TICKETMAP WHERE ROUND=" . $round);
    if (!$deleteResult) {
        $error = db_getErrorMessage($dbconn);
        db_disconnect($dbconn);
        return $error;
    }

    // Create array with field value
    $fieldcount = $state['fieldcount'];
    $fieldarray = array();
    for ( $i = 0; $i < $fieldcount; $i += 1 ) {
        $value = $i + 1;
        $fieldarray[$value] = $value;
    }
    $initialFieldArray = $fieldarray;

    // Map tickets
    $prename = "";
    $surname = "";
    $location = "";
    $tickets = array();
    while($ticketRow = db_fetchRow($ticketResult)) {
        $ticket = $ticketRow[0];
        $ticketprename = $ticketRow[1];
        $ticketsurname = $ticketRow[2];
        $ticketlocation = $ticketRow[3];

        if ($ticketprename !== $prename
                || $ticketsurname !== $surname
                || $ticketlocation !== $location) {
            if (!empty($tickets)) {
                $error = cowpat_mapTickets(
                    $dbconn, $prename, $surname, $location, $tickets, $state);
                if (!empty($error)) {
                    db_disconnect($dbconn);
                    return $error;
                }
                $tickets = array();
            }
            $prename = $ticketprename;
            $surname = $ticketsurname;
            $location = $ticketlocation;
        }
        $tickets[] = $ticket;
    } // while ($ticketRow)

    if (!empty($tickets)) {
        $error = cowpat_mapTickets(
            $dbconn, $prename, $surname, $location, $tickets, $state);
        if (!empty($error)) {
            db_disconnect($dbconn);
            return $error;
        }
        $tickets = array();
    }

    // Write state array to info table
    $infoValues = "";
    foreach ($state as $key => $value) {
        if (!empty($infoValues))
            $infoValues .= ", ";
        $infoValues .= "(" . $round . ", '" . $key . "', '" . $value . "')";
    }
    $infoValuesSql = "INSERT INTO COWPAT.INFO VALUES " . $infoValues;
    if ( !db_query($dbconn, $infoValuesSql) ) {
        $error = "Error in query '" . $infoValuesSql . "': " .
            db_getErrorMessage($dbconn);
        db_query($dbconn, "DELETE FROM COWPAT.INFO WHERE ROUND=" . $round);
        db_disconnect($dbconn);
        return $error;
    }
    
    db_disconnect($dbconn);
    
    $duration = microtime(TRUE) - $time;
    $error .= "initRound took $duration seconds";
    return $error;
}

/**
 * Map tickets of a single owner to to a field.
 * @param[in] dbconn database connection
 * @param[in] prename prename of ticket owner
 * @param[in] surname surname of ticket owner
 * @param[in] location location of ticket owner
 * @param[in] tickets ticket numbers
 * @param[in] roundinfo round information or round number
 */
function cowpat_mapTickets(
        $dbconn, $prename, $surname, $location, $tickets, $roundinfo) {
    if (!is_array($roundinfo)
            || !array_key_exists("id", $roundinfo)
            || !array_key_exists("fieldcount", $roundinfo)) {
        return "Could not map tickets: Invalid round information";
    }

    $fieldcount = $roundinfo['fieldcount'];
    $round = $roundinfo['id'];

    // Create array with field values
    $fieldarray = array();
    for ( $i = 0; $i < $fieldcount; $i += 1 ) {
        $value = $i + 1;
        $fieldarray[$value] = $value;
    }
    
    // Get fields where player is already assigned
    $playerFieldInList = "";
    $playerFieldSql = "SELECT tm.FIELD_ID "
        . "FROM COWPAT.TICKETMAP AS tm "
        . "JOIN COWPAT.TICKETS AS t ON tm.TICKET_ID = t.ID "
        . "WHERE tm.ROUND=" . $round
            . " AND t.PRENAME='" . $prename
            . "' AND t.SURNAME='" . $surname
            . "' AND t.LOCATION='" . $location . "'";
    $assignedResult = db_query($dbconn, $playerFieldSql);
    if (!$assignedResult) {
        return db_getErrorMessage($dbconn);
    }
    while($assignedRow = db_fetchRow($assignedResult)) {
        $field = (int)$assignedRow[0];
        if (!empty($playerFieldInList))
            $playerFieldInList .= ",";
        $playerFieldInList .= $field;
    }

    // Get mapped fields ordered by number of tickets mapped to them
    $fieldDistributionSql = "SELECT FIELD_ID, COUNT(*) AS fieldcount "
        . "FROM COWPAT.TICKETMAP "
        . "WHERE ROUND=" . $round;
    if (!empty($playerFieldInList))
        $fieldDistributionSql .= " AND FIELD_ID NOT IN ("
            . $playerFieldInList . ")";
    $fieldDistributionSql .= " GROUP BY FIELD_ID ORDER BY fieldcount ASC";
    $fieldDistributionResult = db_query($dbconn, $fieldDistributionSql);
    if ( !$fieldDistributionResult ) {
        return "Error executing " . $fieldDistributionSql . ": "
            . db_getErrorMessage($dbconn);
    }

    // Handle empty fields first
    $playerFieldArray = array();
    if ( db_getRowCount($fieldDistributionResult) < $fieldcount ) {
        $ticketMapSql = "SELECT DISTINCT FIELD_ID "
            . "FROM COWPAT.TICKETMAP WHERE ROUND=" . $round;
        $ticketMapResult = db_query($dbconn, $ticketMapSql);
        if (!$ticketMapResult) {
            return "Error executing " . $ticketMapSql . ": "
                . db_getErrorMessage($dbconn);
        } else if ( db_getRowCount($ticketMapResult) < $fieldcount ) {
            $playerFieldArray = $fieldarray;
            while ($ticketMapRow = db_fetchRow($ticketMapResult)) {
                $rowField = (int)$ticketMapRow[0];
                unset($playerFieldArray[$rowField]);
            }
        }
    }
    
    $fieldcount = 0;
    $nextPlayerFieldArray = array();
    $ticketMapValues = "";
    foreach($tickets as $ticket) {
        // Refill playerFieldArray if it is empty
        if (empty($playerFieldArray)) {
            $playerFieldArray = $nextPlayerFieldArray;
            $nextPlayerFieldArray = array();
            while (empty($nextPlayerFieldArray)
                    && ($fieldDistributionRow = db_fetchRow(
                        $fieldDistributionResult))) {
                $rowFieldId = (int)$fieldDistributionRow[0];
                $rowFieldCount = (int)$fieldDistributionRow[1];
                if ($fieldcount === 0 || $fieldcount === $rowFieldCount) {
                    $playerFieldArray[$rowFieldId] = $rowFieldId;
                } else {
                    $nextPlayerFieldArray[$rowFieldId] = $rowFieldId;
                }
                $fieldcount = $rowFieldCount;
            }
            if (empty($playerFieldArray)) {
                $playerFieldArray = $fieldarray;
            }
        }

        $field = array_rand($playerFieldArray);
        unset($playerFieldArray[$field]);
        if (!empty($ticketMapValues))
            $ticketMapValues .= ",";
        $ticketMapValues .= "(" . $round . ", " . $ticket . ", " . $field . ")";
    } // foreach($tickets as $ticket)

    $ticketSql = "INSERT INTO COWPAT.TICKETMAP VALUES " . $ticketMapValues;
    if ( ! db_query($dbconn, $ticketSql) ) {
        return "Error executing " . $ticketSql . ": "
            . db_getErrorMessage($dbconn);
    }

    return "";
}

/**
 * Pause a running round.
 * @param[in] round game round which should be paused
 * @return empty string on success, else error message
 */
function cowpat_pauseRound($round) {
    return cowpat_setField($round, 0);
}

/**
 * Remove a ticket from COWPAT.TICKETS.
 * @param[in] id ticket number
 * @return empty string on success, else error message
 */
function cowpat_removeTicket($id) {
    $error = "";

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    # Delete from tickets
    $sql = "DELETE FROM COWPAT.TICKETS WHERE ID=" . $id ;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    # Delete from round specific ticket map
    $sql = "DELETE FROM COWPAT.TICKETMAP WHERE TICKET_ID=" . $id ;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    db_disconnect($dbconn);
    return $error;
}

/**
 * Reset the round to
 * @param[in] key of the key-value-pair
 * @param[in] value of the key-value-pair
 * @return empty string on success, else error message
 */
function cowpat_resetRound($round) {
    $error = "";

    // Get round
    if (!is_numeric($round)) {
        return "Cannot reset round: parameter is not numeric";
    }

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "DELETE FROM COWPAT.FIELDLOG WHERE ROUND=".$round;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
        db_disconnect($dbconn);
        return $error;
    }

    $sql =  "UPDATE COWPAT.INFO SET VALUE=0 ".
        "WHERE NAME IN ('starttime', 'endtime') " .
        "AND ROUND=". $round;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    db_disconnect($dbconn);    
    return $error;
}

/**
 * Update the cowfield for a defined round.
 * @param round game round
 * @param cowfield new field of cow (0 to pause round)
 */
function cowpat_setField($round, $cowfield) {
    // Check parameters
    if (!is_numeric($cowfield)) {
        return "Cannot set field: Field parameter is not numeric";
    } else if (!is_numeric($round)) {
        return "Cannot set field: Round parameter is not numeric";
    }

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }
    
    // Get lock
    /*$result = db_query($dbconn, "SELECT GET_LOCK('setField',1)");
    if ($result) {
        $row = db_fetchRow($result);
        if ($row[0] != 1) {
            db_disconnect($dbconn);
            return "Error on GET_LOCK: Timeout occured";
        }
    } else {
        db_disconnect($dbconn);
        return "Error on GET_LOCK: " . db_getErrorMessage($dbconn);
    }*/
    
    // Get current state
    $state = array();
    $error = cowpat_getState($state, $round);
    if (strlen($error) > 0) {
        db_disconnect($dbconn);
        return $error;
    } else if (empty($state)) {
        db_disconnect($dbconn);
        return "Cannot set field: Round " . $round . " has no settings.";
    }

    $field = (int)$state['field'];
    $oldfield = (int)$state['oldfield'];
    $fieldcount = (int)$state['fieldcount'];
    $starttime = (int)$state['starttime'];
    $endtime = (int)$state['endtime'];

    if ( $cowfield == 0 && $field == 0 )
        $cowfield = $oldfield;
    
    if (!is_numeric($fieldcount)) {
        $error = "Could not get field count for round " . $round;
    } else if ($cowfield < 0 || $cowfield > $fieldcount) {
        $error = "Could not set field: Value out of range!";
    } else if ($endtime > 0) {
        $error = "Round '" . $round . "' already finished!";
    } else if( $field != $cowfield ) {
        // Get current time
        $enterTime = cowpat_getCurrentTime();
        
        // Write starting time and cowfield to info table
        $sqlField = "UPDATE COWPAT.INFO SET VALUE=" . $cowfield .
            " WHERE ROUND=" . $round . " AND NAME='field'";
        $sqlOldField = "UPDATE COWPAT.INFO SET VALUE=" . $field .
            " WHERE ROUND=" . $round . " AND NAME='oldfield'";

        if ( !db_query($dbconn, $sqlField)
                || !db_query($dbconn, $sqlOldField)) {
            $error = db_getErrorMessage($dbconn);
        }
        //round must be started and field must be changed
        else if( is_numeric($starttime) && $starttime > 0 ) {
            $sqlFieldUpdate = "UPDATE COWPAT.FIELDLOG SET FIELD_LEFT=" .
                $enterTime . " WHERE ROUND=" . $round . " AND FIELD_LEFT IS NULL";
            $sqlFieldInsert = "INSERT INTO COWPAT.FIELDLOG(" .
                "ROUND,FIELD_ID,FIELD_ENTERED,FIELD_LEFT) VALUES" .
                "(".$round.",".$cowfield.",".$enterTime.", NULL)";
            if(!db_query($dbconn, $sqlFieldUpdate)
                    || !db_query($dbconn, $sqlFieldInsert)) {
                $error = db_getErrorMessage($dbconn);
            }
        }
    }
    
    db_disconnect($dbconn);
    return $error;
}

/**
 * Save an instruction for the frontend
 * @param[in] key of the key-value-pair
 * @param[in] value of the key-value-pair
 * @return empty string on success, else error message
 */
function cowpat_saveInstruction($name, $value) {
    $dbconn = db_connect();
    $name=db_realEscapeString($dbconn, $name);
    $value=db_realEscapeString($dbconn, $value);
    $error="";

    $sql = "UPDATE INSTRUCTIONS SET VALUE='$value' WHERE NAME='$name'";
    $result = db_query($dbconn, $sql);
    if(!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    db_disconnect($dbconn);
    
    return $error;
}

/**
 * Set winners of a round.
 * Round must be stopped to make this method succeed.
 * @param round round which is updated
 * @param winnerArray array of winner ticket numbers
 * @return empty string on success, else error message
 */
function cowpat_setWinners($round, $winnerArray) {
    $error = "";

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    // Check if round is started
    $sql = "SELECT VALUE FROM COWPAT.INFO " .
        "WHERE ROUND=" . $round . " AND NAME='endtime'";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $error = db_getErrorMessage($dbconn);
    } else if (db_getRowCount($result) === 0) {
        $error = "Unknown round '" . $round . "'!";
    } else {
        $row = db_fetchRow($result);
        if ((int)($row[0]) === 0) {
            $error = "Round '" . $round . "' is not yet finished!";
        }
    }
    if (strlen($error) > 0) {
        db_disconnect($dbconn);
        return $error;
    }

    $winnerid = 1;
    $winnercount = sizeof($winnerArray);
    foreach($winnerArray as $ticketid) {
        $sql = "INSERT INTO COWPAT.INFO VALUES (" .
            $round . ", 'winner_" . $winnerid . "', " . $ticketid . ")";
        $result = db_query($dbconn, $sql);
        if (!$result) {
            $db_error = db_getErrorMessage($dbconn);
            $error = "db_query() failed for query '" . $sql . "': " . $db_error;
            break;
        }
        $winnerid += 1;
    }

    return $error;
}

/**
 * Start a new round.
 * $param[in] round number of round which should be started
 * @param[in] cowfield starting field of cow
 * @return empty string on success, else error message
 */
function cowpat_startRound($round, $cowfield ) {
    $error = "";

    // Connect to DB
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    // Check if round is initialized
    $sql = "SELECT VALUE FROM COWPAT.INFO " .
        "WHERE ROUND=" . $round . " AND NAME='starttime'";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $error = db_getErrorMessage($dbconn);
    } else if (db_getRowCount($result) === 0) {
        $error = "Unknown round '" . $round . "'!";
    } else {
        $row = db_fetchRow($result);
        if ((int)($row[0]) !== 0) {
            $error = "Round '" . $round . "' already started!";
        }
    }
    if (strlen($error) > 0) {
        db_disconnect($dbconn);
        return $error;
    }

    // Get current time
    $time = cowpat_getCurrentTime();
    // Write starting time and cowfield to info table
    if (!is_numeric($cowfield) || $cowfield<=0)
        $cowfield="VALUE";

    $sqlClearFieldlog = "DELETE FROM COWPAT.FIELDLOG WHERE ROUND=".$round;
    $sqlInsertFieldlog = "INSERT INTO COWPAT.FIELDLOG(ROUND,FIELD_ID,FIELD_ENTERED,FIELD_LEFT) VALUES" .
        "(".$round.",".$cowfield.",".$time.",NULL)";

    $sqlField = "UPDATE COWPAT.INFO SET VALUE=" . $cowfield .
        " WHERE ROUND=" . $round . " AND NAME='field'";
    $sqlStartTime = "UPDATE COWPAT.INFO SET VALUE=" . $time .
        " WHERE ROUND=" . $round . " AND NAME='starttime'";
    if ( !db_query($dbconn, $sqlClearFieldlog)
            || !db_query($dbconn, $sqlInsertFieldlog)
            || !db_query($dbconn, $sqlField)
            || !db_query($dbconn, $sqlStartTime) ) {
        $error = "Something went wrong: " . db_getErrorMessage($dbconn);
        db_disconnect($dbconn);
        return $error;
    }

    db_disconnect($dbconn);
    return $error;
}

/**
 * Stop a running round.
 * @param[in] round game round which should be stopped
 * @return empty string on success, else error message
 */
function cowpat_stopRound($round) {
    $error = "";
    $starttime=0;

    // Get round
    if (!is_numeric($round)) {
        return "Cannot stop round: parameter is not numeric";
    }

    // Update time according to start, end and maximum time
    $state = array();
    $error = cowpat_getState($state, $round);
    if (strlen($error) > 0) {
        return $error;
    } else if (empty($state)) {
        return "Unknown round " . $round;
    }

    $starttime = (int)$state['starttime'];
    $endtime = (int)$state['endtime'];
    $maxtime = (int)$state['maxtime'];
    if ($endtime > 0)
        return "Cannot stop round: Round " . $round . " already finished!";
    else if ($starttime == 0)
        return "Cannot stop round: Round " . $round . " not yet started!";
    
    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    // Get current time
    $time = cowpat_getCurrentTime();

    $pausetime = 0;
    $error = cowpat_getTimeOnField($pausetime, $round, 0, $time);
    if (strlen($error) > 0)
        return $error;

    $difftime = $time - $starttime -$pausetime;
    if( $difftime > $maxtime ) {
        $time=$starttime+$pausetime+$maxtime;
    }

    // Set stop time
    $sqlFieldTime = "UPDATE COWPAT.INFO SET VALUE=" . $time .
        " WHERE ROUND=" . $round . " AND NAME='endtime'";
    if (!db_query($dbconn, $sqlFieldTime)) {
        $error = db_getErrorMessage($dbconn);
        db_disconnect($dbconn);
        return $error;
    }        

    db_disconnect($dbconn);
    return $error;
}

/**
 * Update a ticket.
 * @param[in] id ticket number
 * @param[in] prename prename of ticket owner
 * @param[in] surname surname of ticket owner
 * @param[in] location location of ticket owner
 * @return empty string on success, else error message
 */
function cowpat_updateTicket($id, $prename, $surname, $location) {
    $error = "";

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    $sql = "UPDATE COWPAT.TICKETS SET PRENAME='" . $prename .
        "', SURNAME='" . $surname . "', LOCATION='" . $location . "' " .
        "WHERE ID=" . $id;
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $db_error = db_getErrorMessage($dbconn);
        $error = "db_query() failed for query '" . $sql . "': " . $db_error;
    }

    db_disconnect($dbconn);
    return $error;
}

/**
 * Get the game status.
 * @param[in] Number of the round
 */
function cowpat_turnBackTime($round) {
    $error = "";
    $endtime_s;
    $starttime_s;

    // Get round
    if (!is_numeric($round)) {
        return "Cannot turn back time for round: parameter is not numeric";
    }

    $dbconn = db_connect();
    if (!$dbconn) {
        return "db_connect() failed";
    }

    //get the endtime
    $sql = "SELECT VALUE FROM COWPAT.INFO " .
        "WHERE ROUND=" . $round . " AND NAME='endtime'";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $error = db_getErrorMessage($dbconn);
    } else if (db_getRowCount($result) === 0) {
        $error = "Unknown round '" . $round . "'!";
    } else {
        $row = db_fetchRow($result);
        if ((int)($row[0]) === 0) {
            $error = "Round '" . $round . "' is not yet finished!";
        }
        $endtime = $row[0];
    }
    if (strlen($error) > 0) {
        db_disconnect($dbconn);
        return $error;
    }

    //get the starttime
    $sql = "SELECT VALUE FROM COWPAT.INFO " .
        "WHERE ROUND=" . $round . " AND NAME='starttime'";
    $result = db_query($dbconn, $sql);
    if (!$result) {
        $error = db_getErrorMessage($dbconn);
    } else if (db_getRowCount($result) === 0) {
        $error = "Unknown round '" . $round . "'!";
    } else {
        $row = db_fetchRow($result);
        if ((int)($row[0]) === 0) {
            $error = "Round '" . $round . "' is not yet started!";
        }
        $starttime = $row[0];
    }
    if (strlen($error) > 0) {
        db_disconnect($dbconn);
        return $error;
    }

    //set the new endtime
    $endtime-=1000;
    if( $endtime<=$starttime ) {
        db_disconnect($dbconn);
        return "endtime must not be smaller then starttime";
    }

    $sqlFieldTime = "UPDATE COWPAT.INFO SET VALUE=" . $endtime .
        " WHERE ROUND=" . $round . " AND NAME='endtime'";
    if (!db_query($dbconn, $sqlFieldTime)) {
        $error = db_getErrorMessage($dbconn);
        db_disconnect($dbconn);
        return $error;
    }

    //delete all fields entered after game end
    $sqlFieldLogRollback = "DELETE FROM COWPAT.FIELDLOG"
        . " WHERE ROUND=" .$round
        . " AND FIELD_ENTERED>". $endtime;
    if(!db_query($dbconn, $sqlFieldLogRollback)) {
        $error = db_getErrorMessage($dbconn);
        if (strlen($error) > 0) {
            db_disconnect($dbconn);
            return $error;
        }
    }

    //get latest field from fieldlog
    $last_field;
    $sqlGetLastField = "SELECT FIELD_ID FROM COWPAT.FIELDLOG WHERE ROUND="
        .$round . " AND FIELD_ID > 0 ORDER BY ID DESC LIMIT 1";
    $result = db_query($dbconn, $sqlGetLastField);
    if (!$result) {
        $error = db_getErrorMessage($dbconn);
    } else {
        $row = db_fetchRow($result);
        $last_field = $row[0];
    }
    if (strlen($error) > 0) {
        db_disconnect($dbconn);
        return $error;
    }

    //update field in the info table
    if(isset($last_field)) {
        $sqlFieldUpdate = "UPDATE COWPAT.INFO SET VALUE=" . $last_field .
            " WHERE ROUND=" . $round . " AND NAME='field'";
        if (!db_query($dbconn, $sqlFieldUpdate)) {
            $error = db_getErrorMessage($dbconn);
            db_disconnect($dbconn);
            return $error;
        }
    } 

    

    db_disconnect($dbconn);
    return $error ;
}
?>
