<?php
// Some functions used for db handling
function db_connect() {
    $dbhost = "localhost";
    $dbuser ="oldmcdonald";
    $dbpw = "hadacow";
    $dbschema = "COWPAT";

    //$dbconnection = mysql_connect($dbhost,$dbuser,$dbpw, TRUE);
    //if ($dbconnection) {
    //    $dbselection = mysql_select_db($dbschema, $dbconnection);
    //    if ($dbselection) {
    //        return $dbconnection;
    //    }
    //}
    mysqli_report(MYSQLI_REPORT_ERROR);
    $dbconnection = mysqli_connect($dbhost,$dbuser,$dbpw, $dbschema);
    if ($dbconnection) {
        return $dbconnection;
    }
    return FALSE;
}

function db_disconnect($dbconnection) {
    // Disconnect from DB
    if ($dbconnection) {
        //return mysql_close($dbconnection);
        return mysqli_close($dbconnection);
    } else {
        return FALSE;
    }
}

function db_fetchRow($resultset) {
    // Get next row from result set or FALSE in case of end of set
    //return mysql_fetch_row($resultset);
    return mysqli_fetch_row($resultset);
}

function db_getErrorMessage($dbconnection) {
    // Get last error as error message
    if ($dbconnection) {
        //$error = "MySQL error # " . mysql_errno($dbconnection) . ": " .
        //    mysql_error($dbconnection);
        $error = "MySQL error # " . mysqli_errno($dbconnection) . ": " .
            mysqli_error($dbconnection);
        return $error;
    } else {
        return "";
    }
}

function db_getRowCount($resultset) {
    // Get row count of query result
    //return mysql_num_rows($resultset);
    return mysqli_num_rows($resultset);
}

function db_query($dbconnection, $sql) {
    // Query DB and get result set
    //return mysql_query($sql, $dbconnection);
    return mysqli_query($dbconnection, $sql);
}

function db_realEscapeString($dbconnection, $string) {
    //return mysql_real_escape_string($string);
    return mysqli_real_escape_string($dbconnection, $string);
}

?>
