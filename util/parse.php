<?php
/**
 * Parse the ticket array and return a string representation.
 * @param[in] tickets ticket array
 * @param[in] delimiter delimiter used to separate ticket numbers
 * @return string representation of tickets
 */
function parse_ticketArray($tickets, $delimiter = ",") {
    $string = "";

    $range = 0;
    $rangeend = FALSE;
    
    foreach($tickets as $ticket) {
        if ($range > 0) {
            if ($ticket == ($rangeend + 1)) {
                $rangeend = $ticket;
                ++$range;
                continue;
            } else if ($range > 1) {
                $string .= "-$rangeend";
            }
            $string .= "$delimiter ";
        }

        $string .= $ticket;
        $range = 1;
        $rangeend = $ticket;
    }

    if ($range > 1) {
        $string .= "-$rangeend";
    }

    return $string;
}

/**
 * Parse the ticket string and return an array of tickets.
 * @param[out] tickets array of tickets from string
 * @param[in] string ticket string which is parsed
 * @param[in] delimiter delimiter used to separate ticket numbers
 * @return empty string on success, else error message
 */
function parse_ticketString(&$tickets, $string, $delimiter = ",") {
    $tokens = explode($delimiter, $string);
    
    $tickets = array();

    foreach($tokens as $token) {
        $rangetokens = explode("-", $token);
        if (sizeof($rangetokens) == 2) {
            if (!is_numeric($rangetokens[0])) {
                return "Invalid ticket id '" . $rangetokens[0] . "'";
            } else if (!is_numeric($rangetokens[1])) {
                return "Invalid ticket id '" . $rangetokens[1] . "'";
            }

            $minValue = (int)$rangetokens[0];
            $maxValue = (int)$rangetokens[1];
            if ($maxValue < $minValue) {
                return "Invalid ticket range entry '" . $minValue . " - " .
                    $maxValue . "'";
            }
            for ($i = $minValue; $i <= $maxValue; $i++) {
                $tickets[] = $i;
            }
        } else if (sizeof($rangetokens) == 1 && is_numeric($token)) {
            $tickets[] =  (int) $token;
        } else {
            return "Invalid ticket id '" . $token . "'";
        }
    }

    return "";
}
?>

